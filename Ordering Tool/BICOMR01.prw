#INCLUDE "TOTVS.CH"
#include "apwizard.ch"
#Include "Fileio.ch"

/*/{Protheus.doc} BICOMR01
Exporta relatorio de sugestao de compra por fornecedores

@type       User Function
@author     Mauro Paladini
/*/

User Function BICOMR01()
   
    Local oWizard       := Nil

    Local cTitulo       := "Assistente de Exporta��o"
    Local nLin          := 0
    Local cStepDesc1    := ""
    
    Local aUserInfo     := GetUserInfoArray()
    Local nPosUsr       := aScan( aUserInfo,{ |x,y| x[3] == ThreadId() } )
    Local cComputer     := RTrim( aUserInfo[nPosUsr][2] )    
    
    Local lConfirma     := .F.
    Local cWelcomeMsg   := ""

    Local oArial12      := TFont():New('Arial',,-12,,.F.,,,,)
    Local oArial12B     := TFont():New('Arial',,-12,,.T.,,,,)
    
    Local oVerdana10B   := TFont():New('Verdana',,-10,,.T.,,,,)
    Local oVerdana10    := TFont():New('Verdana',,-10,,.F.,,,,)

    Private oListFiles  := Nil  
    Private aListFiles  := {}      
    Private nPosArq     := 1    
    Private lAbandonar  := .F.

    Private oTextBar01
    Private oTextBar02  
    Private oTextBar03
    Private oMeter01
    Private oMeter02    
    Private cTextBar01
    Private cTextBar02
    Private cTextBar03
    Private nActual01 
    Private nActual02  
    
    Private oBackGround
    Private lAbandonar      := .F.
    Private cArquivo        := Space(200)
    Private oGetArquivo
    Private aRotina         := {}
    
  
    cWelcomeMsg := "Ao assistente de exporta��o de arquivos." + CRLF + "As opera��es realizadas neste assistente ter�o como base a FILIAL logada no sistema." + CRLF + CRLF + CRLF

    DEFINE WIZARD oWizard TITLE "Allfood" HEADER "" MESSAGE " " TEXT cWelcomeMsg PANEL NEXT {|| .T. } FINISH {|| .F. }

    //-------------------------PAINEL 2: Seleciona Opera��o -----------------------------------------//

    cStepDesc1  := ""
    CREATE PANEL oWizard HEADER cTitulo MESSAGE cStepDesc1 ;
        BACK {|| .T. } NEXT {|| VldStepWiz() } FINISH {|| lConfirma := .T. , .T. } EXEC {|| .T. } PANEL

    @ 09, 15 Say "Indique o caminho onde deseja exportar:" Size 200,009 COLOR CLR_HBLUE Of oWizard:oMPanel[2] Pixel

    DEFINE SBUTTON oBut3 FROM 20, 15 TYPE 14 ACTION ( cArquivo := WizGetFile() ) ENABLE of oWizard:oMPanel[2]

    @ 20, 45 MSGET oGetArquivo VAR cArquivo  SIZE 225, 10  VALID .T. OF oWizard:oMPanel[2] PIXEL
            
    ACTIVATE WIZARD oWizard CENTERED
    
    If lConfirma
    
        P05->( DbSetOrder(1) )
        IF P05->( DbSeek( xFilial('P05') + P00->P00_ID ) )
        
            While P05->( !Eof() .And. P05_FILIAL+P05_IDPROC == xFilial('P05') + P00->P00_ID )
            
                // -- Faz tratamento no nome do arquivo
                
                
        
                // -- Fazer a chamada por fabricante 
                PrepExport( cArquivo , P00->P00_ID , cCodFabric )            
            
                P05->( DbSkip() )            

            End

        Endif         
        
    Endif

Return





/*
WiVldFinish
Valida a finalizacao do wizard

@author Mauro Paladini
@since  27/10/2016
@version 1.0
*/

Static Function WiVldFinish()

    Local lRet  := MsgYesNo("Confirma a exporta��o ? ")

Return lRet




/*
VldStepWiz
Funcao generica para a validacao do Step do Wizard

@author Mauro Paladini
@since  27/10/2016
@version 1.0
*/
Static Function VldStepWiz()

    Local lRet  := .T.

    IF Empty(cArquivo)
    
        lRet    := .F.
        MsgInfo("Voc� deve indicar local para a exporta��o do arquivo")
    
    Endif
    
Return lRet






/*
WizGetFile
Funcao generica para informar o path do arquivo

@author Mauro Paladini
@since  27/10/2016
@version 1.0
*/
                          
Static Function WizGetFile()
          
    Local cArquivo := ""
    Local cFile    := ""
    Local cExten   := ""

    Local cMascara  := "Arquivos   (*.xml) |*.xml| "
    Local cTitulo   := "Informe um nome "
    Local nMascpad  := 0
    Local cDirini   := "\"
    Local lSalvar   := .T.              /*.T. = Salva || .F. = Abre*/
    Local nOpcoes   := GETF_LOCALHARD
    Local lArvore   := .F.              /*.T. = apresenta o �rvore do servidor || .F. = n�o apresenta*/

    cArquivo := Upper( cGetFile( cMascara , cTitulo , nMascpad , cDirIni , lSalvar , nOpcoes , lArvore ) )
    
    IF Right( Upper ( cArquivo ) , 4 ) <> '.XML'    
        cArquivo += '.XML '    
    Endif 

Return( Upper ( cArquivo ) )


/*
PrepExport
Realiza a exporta��o do arquivo 

@author Mauro Paladini
@since  27/10/2016
@version 1.0
*/

Static Function PrepExport( cArqUser , cIdProc , cCodFabric )

Local cArqRel   := CriaTrab( Nil , .F. ) + '.xml'
Local cTrb      := GetNextAlias()

Local nTotPC    := 0
Local nTotKG    := 0
Local nX        := 0
Local aSupplier := {}

Local lCopy     := .F.

Private hArquivo  := 0

P01->( DbSetOrder(1) )
P01->( DbSeek( xFilial('P01') + cIdProc ) )

// -- QUERY

BEGINSQL Alias cTrb

    SELECT A5_FORNECE, A5_LOJA, P01_COD, A5_PRODUTO, A5_CODPRF, P01_QTDPED, B1_UM, B1_SEGUM, P01_QTDPED, P01_DESPRO, 
            
            CASE    WHEN  SB1.B1_TIPCONV = 'M'  
                        THEN  P01.P01_QTDPED * SB1.B1_CONV      
                    ELSE  
                        P01.P01_QTDPED /  SB1.B1_CONV       
                    END As P01_QTDPED2
    
    FROM %table:SA5% SA5 
    
    INNER JOIN %table:SB1% SB1
    ON B1_FILIAL = %xFilial:SB1% AND SB1.B1_COD = SA5.A5_PRODUTO
    
    INNER JOIN %table:P01% P01
    ON P01.P01_FILIAL = %xFilial:P01% AND P01.P01_IDPROC = %exp:cIdProc% AND P01.P01_COD = SA5.A5_PRODUTO
    
    WHERE   SA5.A5_FILIAL = %xFilial:SA5% AND      
            SA5.A5_MSBLQL <> '1' AND 
            SA5.D_E_L_E_T_ = '' AND
            SB1.D_E_L_E_T_ = '' AND 
            P01.P01_QTDPED > 0  AND 
            P01.P01_CODFAB = %exp:cCodFabric% AND
            P01.D_E_L_E_T_ = ''
        
ENDSQL

(cTrb)->( DbGoTop() )

IF (cTrb)->( !Eof() )

    SA2->( DbSetOrder(1) )
    IF SA2->( DbSeek( xFilial('SA2') + (cTrb)->A5_FORNECE + (cTrb)->A5_LOJA ) )
        
        aAdd( aSupplier , RTRIM(SA2->A2_NREDUZ) )
        
        aAux := Separa( RTRIM(SA2->A2_END) , '-' )        
        For nX := 1 To Len(aAux )
            aAdd( aSupplier , aAux[nX] )        
        Next nX 
        
    Endif

Endif


If Len(aSupplier) == 0
    ApMsgStop( 'Os dados do fabricante n�o foram localizados', 'ATEN��O' )
    aSupplier   :=  { '', '', '', '' }
Endif


// -- Faz a gera��o do relatorio no servidor e ap�s o processamento faz a c�pia
// do arquivo para o local selecionado pelo usu�rio 

hArquivo := FCreate( AllTrim(cArqRel) , FC_NORMAL )

If ( hArquivo != -1 )
            
    // -- Gera Cabecalho do arquivo
    PrepCab( hArquivo , cArqRel , aSupplier )
    
    // -- Gera itens do Pedido
    
    (cTrb)->( DbGoTop() )
        
    While (cTrb)->( !EOF() )
        
        PrepItem( hArquivo , cTrb , @nTotKG , @nTotPC )
        
        (cTrb)->( DbSkip() )
        
    End
    
    // -- Grava total dos itens 
    
    PrepTotal( hArquivo , @nTotKG , @nTotPC )
    
Endif

// -- Grava Fim da Planilha 
 
PrepFim( hArquivo )


// -- Tratamento para copiar os arquivos para o diretorio local 

lCopy  := CpyS2T( cArqRel , cArqUser )

// -- END Query

(cTrb)->( DbClosearea() )

Return



/*/{Protheus.doc} PrepItem
Prepara os itens da planilha

@type       Static Function
@author     Mauro Paladini
/*/

Static Function PrepItem( hArquivo , cTrb , nTotKG , nTotPC )

Local cXML  := ''

cXML +=  '   <Row ss:StyleID="s39">'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s58"><Data ss:Type="String">' + RTRIM((cTrb)->A5_CODPRF) + '</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s58"><Data ss:Type="String">'   + RTRIM((cTrb)->P01_COD )        +  '</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s59"><Data ss:Type="String">'   + RTRIM((cTrb)->P01_DESPRO )     + '</Data></Cell>'

IF (cTrb)->B1_UM == 'KG'

    cXML +=  '    <Cell ss:StyleID="s213"><Data ss:Type="String">'  + RTRIM( Transform( (cTrb)->P01_QTDPED     ,"@E 999,999.99") ) +   '</Data></Cell>'
    cXML +=  '    <Cell ss:StyleID="s61"><Data ss:Type="String">'   + RTRIM( Transform( (cTrb)->P01_QTDPED2    ,"@E 999,999.99") )+   '</Data></Cell>'

    nTotKG  += (cTrb)->P01_QTDPED
    nTotPC  += (cTrb)->P01_QTDPED2


Else

    cXML +=  '    <Cell ss:StyleID="s213"><Data ss:Type="String">'  + RTRIM( Transform( (cTrb)->P01_QTDPED2    ,"@E 999,999.99") ) +   '</Data></Cell>'
    cXML +=  '    <Cell ss:StyleID="s61"><Data ss:Type="String">'   + RTRIM( Transform( (cTrb)->P01_QTDPED     ,"@E 999,999.99") )+   '</Data></Cell>'
    
    nTotKG  += (cTrb)->P01_QTDPED2
    nTotPC  += (cTrb)->P01_QTDPED

Endif

cXML +=  '   </Row>'

FWrite( hArquivo, cXML , Len(cXML) + 2 )

Return


/*/{Protheus.doc} PrepTotal()
Prepara o Total da Planilha

@type       Static Function
@author     Mauro Paladini
/*/

Static Function PrepTotal( hArquivo , nTotKG , nTotPC )

Local cXML  := ''

cXML +=  '   <Row ss:StyleID="s40">'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s58"/>'
cXML +=  '    <Cell ss:StyleID="s58"/>'
cXML +=  '    <Cell ss:StyleID="s59"/>'
cXML +=  '    <Cell ss:StyleID="s213"/>'
cXML +=  '    <Cell ss:StyleID="s61"/>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="4" ss:StyleID="s18"><Data ss:Type="String">TOTALE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s43"><Data ss:Type="String">' + RTRIM( Transform( nTotKG    ,"@E 999,999.99") ) + '</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s44"><Data ss:Type="String">' + RTRIM( Transform( nTotPC    ,"@E 999,999.99") ) + '</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="4" ss:StyleID="s18"/>'
cXML +=  '    <Cell ss:StyleID="s45"/>'
cXML +=  '    <Cell ss:StyleID="s45"/>'
cXML +=  '    <Cell ss:Index="10" ss:StyleID="s47"/>'
cXML +=  '    <Cell ss:StyleID="s48"/>'
cXML +=  '   </Row>'

FWrite( hArquivo, cXML , Len(cXML) + 2 )

Return


/*/{Protheus.doc} PrepCab
Prepara cabecalho do arquivo 

@type       Static Function
@author     Mauro Paladini
/*/

Static Function PrepCab( hArquivo , cArqRel , aSupplier )

Local cXML := ''

cXML +=  '<?xml version="1.0"?>'
cXML +=  '<?mso-application progid="Excel.Sheet"?>'
cXML +=  '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"'
cXML +=  ' xmlns:o="urn:schemas-microsoft-com:office:office"'
cXML +=  ' xmlns:x="urn:schemas-microsoft-com:office:excel"'
cXML +=  ' xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'
cXML +=  ' xmlns:html="http://www.w3.org/TR/REC-html40">'
cXML +=  ' <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">'
cXML +=  '  <Author>ALLFOOD</Author>'
cXML +=  '  <LastAuthor>ALFOOD</LastAuthor>'
cXML +=  '  <LastPrinted>2018-10-02T19:38:26Z</LastPrinted>'
cXML +=  '  <Created>2018-10-01T16:30:52Z</Created>'
cXML +=  '  <LastSaved>2018-10-02T19:47:58Z</LastSaved>'
cXML +=  '  <Version>14.00</Version>'
cXML +=  ' </DocumentProperties>'
cXML +=  ' <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">'
cXML +=  '  <AllowPNG/>'
cXML +=  ' </OfficeDocumentSettings>'
cXML +=  ' <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">'
cXML +=  '  <WindowHeight>7500</WindowHeight>'
cXML +=  '  <WindowWidth>20115</WindowWidth>'
cXML +=  '  <WindowTopX>240</WindowTopX>'
cXML +=  '  <WindowTopY>90</WindowTopY>'
cXML +=  '  <ProtectStructure>False</ProtectStructure>'
cXML +=  '  <ProtectWindows>False</ProtectWindows>'
cXML +=  ' </ExcelWorkbook>'
cXML +=  ' <Styles>'
cXML +=  '  <Style ss:ID="Default" ss:Name="Normal">'
cXML +=  '   <Alignment ss:Vertical="Bottom"/>'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>'
cXML +=  '   <Interior/>'
cXML +=  '   <NumberFormat/>'
cXML +=  '   <Protection/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s16" ss:Name="Hyperlink">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#0000FF"'
cXML +=  '    ss:Underline="Single"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s17">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s18">'
cXML +=  '   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s19">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s20">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#FF0000"'
cXML +=  '    ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s22">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#FF0000"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s24">'
cXML +=  '   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <NumberFormat ss:Format="dd/mm/yy"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s32">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s33">'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s39">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s40">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"'
cXML +=  '    ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s43">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <NumberFormat ss:Format="#,##0.0"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s44">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <NumberFormat ss:Format="#,##0"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s45">'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <NumberFormat ss:Format="#,##0.0"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s47">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <NumberFormat ss:Format="Standard"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s48">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s49">'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s55">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s58">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>'
cXML +=  '   <Interior/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s59">'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>'
cXML +=  '   <Interior/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s61">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>'
cXML +=  '   <Interior/>'
cXML +=  '   <NumberFormat ss:Format="#,##0"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s144">'
cXML +=  '   <Alignment ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"'
cXML +=  '    ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s149">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>'
cXML +=  '   <Interior/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s150">'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s157">'
cXML +=  '   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s161">'
cXML +=  '   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s163" ss:Parent="s16">'
cXML +=  '   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#0000FF"'
cXML +=  '    ss:Bold="1" ss:Underline="Single"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '   <Protection/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s167">'
cXML +=  '   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s168">'
cXML +=  '   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s169">'
cXML +=  '   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s182">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s210">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#FFFFFF"'
cXML +=  '    ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#002060" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s212">'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#FFFFFF"'
cXML +=  '    ss:Bold="1"/>'
cXML +=  '   <Interior ss:Color="#002060" ss:Pattern="Solid"/>'
cXML +=  '  </Style>'
cXML +=  '  <Style ss:ID="s213">'
cXML +=  '   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>'
cXML +=  '   <Borders>'
cXML +=  '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>'
cXML +=  '   </Borders>'
cXML +=  '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>'
cXML +=  '   <Interior/>'
cXML +=  '   <NumberFormat ss:Format="#,##0.0"/>'
cXML +=  '  </Style>'
cXML +=  ' </Styles>'
cXML +=  ' <Worksheet ss:Name="Sheet1">'
cXML +=  '  <Table ss:ExpandedColumnCount="16132" ss:ExpandedRowCount="17" x:FullColumns="1"'
cXML +=  '   x:FullRows="1" ss:StyleID="s17" ss:DefaultRowHeight="15.75">'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="20.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="80.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="570"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="106.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="98.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:Width="70.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:Index="245" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="247" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="251" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="501" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="503" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="507" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="757" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="759" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="763" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="1013" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="1015" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="1019" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="1269" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="1271" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="1275" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="1525" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="1527" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="1531" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="1781" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="1783" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="1787" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="2037" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="2039" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="2043" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="2293" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="2295" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="2299" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="2549" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="2551" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="2555" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="2805" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="2807" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="2811" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="3061" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="3063" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="3067" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="3317" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="3319" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="3323" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="3573" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="3575" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="3579" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="3829" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="3831" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="3835" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="4085" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="4087" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="4091" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="4341" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="4343" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="4347" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="4597" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="4599" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="4603" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="4853" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="4855" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="4859" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="5109" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="5111" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="5115" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="5365" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="5367" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="5371" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="5621" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="5623" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="5627" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="5877" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="5879" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="5883" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="6133" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="6135" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="6139" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="6389" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="6391" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="6395" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="6645" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="6647" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="6651" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="6901" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="6903" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="6907" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="7157" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="7159" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="7163" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="7413" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="7415" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="7419" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="7669" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="7671" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="7675" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="7925" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="7927" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="7931" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="8181" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="8183" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="8187" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="8437" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="8439" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="8443" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="8693" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="8695" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="8699" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="8949" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="8951" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="8955" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="9205" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="9207" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="9211" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="9461" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="9463" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="9467" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="9717" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="9719" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="9723" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="9973" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="9975" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="9979" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="10229" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="10231" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="10235" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="10485" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="10487" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="10491" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="10741" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="10743" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="10747" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="10997" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="10999" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="11003" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="11253" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="11255" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="11259" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="11509" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="11511" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="11515" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="11765" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="11767" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="11771" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="12021" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="12023" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="12027" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="12277" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="12279" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="12283" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="12533" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="12535" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="12539" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="12789" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="12791" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="12795" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="13045" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="13047" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="13051" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="13301" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="13303" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="13307" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="13557" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="13559" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="13563" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="13813" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="13815" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="13819" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="14069" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="14071" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="14075" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="14325" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="14327" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="14331" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="14581" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="14583" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="14587" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="14837" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="14839" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="14843" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="15093" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="15095" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="15099" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="15349" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="15351" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="15355" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="15605" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="15607" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="15611" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="15861" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="15863" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="15867" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Column ss:Index="16117" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="72.75"'
cXML +=  '    ss:Span="1"/>'
cXML +=  '   <Column ss:Index="16119" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="280.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="2"/>'
cXML +=  '   <Column ss:Index="16123" ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="101.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="92.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="85.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="21.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="58.5"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="60.75"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="65.25"/>'
cXML +=  '   <Column ss:StyleID="s17" ss:AutoFitWidth="0" ss:Width="57.75"/>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="4" ss:MergeAcross="1" ss:StyleID="s55"/>'
cXML +=  '    <Cell ss:StyleID="s55"/>'
cXML +=  '    <Cell ss:StyleID="s55"/>'
cXML +=  '    <Cell ss:Index="10" ss:StyleID="s49"/>'
cXML +=  '   </Row>'
cXML +=  '   <Row ss:Height="18.75">'
cXML +=  '    <Cell ss:StyleID="s161"/>'
cXML +=  '    <Cell ss:StyleID="s169"><Data ss:Type="String">SUPPLIER</Data></Cell>'
cXML +=  '    <Cell ss:Index="5" ss:StyleID="s19"/>'
cXML +=  '    <Cell ss:StyleID="s157"><Data ss:Type="String">ALLFOOD IMPORT., IND. E COM. S.A</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:StyleID="s161"/>'
cXML +=  '    <Cell ss:StyleID="s167"><Data ss:Type="String">' + aSupplier[1] + '</Data></Cell>'
cXML +=  '    <Cell ss:Index="6" ss:StyleID="s168"><Data ss:Type="String">Rua Presidente Costa Pereira, 373</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:StyleID="s161"/>'
cXML +=  '    <Cell ss:StyleID="s167"><Data ss:Type="String">' + aSupplier[2] + '</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s19"/>'
cXML +=  '    <Cell ss:StyleID="s55"/>'
cXML +=  '    <Cell ss:StyleID="s20"/>'
cXML +=  '    <Cell ss:StyleID="s168"><Data ss:Type="String">Mooca  CEP 03108-040</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:StyleID="s161"/>'
cXML +=  '    <Cell ss:StyleID="s167"><Data ss:Type="String">' + aSupplier[3] + '</Data></Cell>'
cXML +=  '    <Cell ss:Index="4" ss:StyleID="s144"/>'
cXML +=  '    <Cell ss:StyleID="s20"/>'
cXML +=  '    <Cell ss:StyleID="s168"><Data ss:Type="String">S�o Paulo SP � Brasil</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:StyleID="s161"/>'
cXML +=  '    <Cell ss:StyleID="s167"/>'
cXML +=  '    <Cell ss:Index="4" ss:StyleID="s144"/>'
cXML +=  '    <Cell ss:StyleID="s20"/>'
cXML +=  '    <Cell ss:StyleID="s168"><Data ss:Type="String">Tel/Fax: 55 11 27990422</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="4" ss:StyleID="s144"/>'
cXML +=  '    <Cell ss:StyleID="s22"/>'
cXML +=  '    <Cell ss:StyleID="s163"><Data ss:Type="String">importacao@allfood.com.br</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row ss:Height="18.75">'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s150"><Data ss:Type="String">P.O. DATE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s19"/>'
cXML +=  '    <Cell ss:StyleID="s182"><Data ss:Type="String">OUR P.O. REFERENCE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s19"/>'
cXML +=  '    <Cell ss:StyleID="s157"/>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s24"><Data ss:Type="String">' + DTOC(P00->P00_DTCALC)+ '</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s24"/>'
cXML +=  '    <Cell ss:StyleID="s149"><Data ss:Type="String">' + ALLTRIM( UPPER(P00->P00_OBS) ) + '</Data></Cell>'
cXML +=  '    <Cell ss:Index="7" ss:StyleID="s19"/>'
cXML +=  '    <Cell ss:StyleID="s55"/>'
cXML +=  '    <Cell ss:StyleID="s55"/>'
cXML +=  '   </Row>'
cXML +=  '   <Row ss:Index="11">'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s210"><Data ss:Type="String" x:Ticked="1">CODICE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">CODICE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">DESCRIZIONE DELLA MERCE</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">PESO</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">TOTALE</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s210"><Data ss:Type="String">PROD.</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">ALL</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s212"/>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">IN KILI</Data></Cell>'
cXML +=  '    <Cell ss:StyleID="s210"><Data ss:Type="String">PEZZI</Data></Cell>'
cXML +=  '   </Row>'
cXML +=  '   <Row>'
cXML +=  '    <Cell ss:Index="2" ss:StyleID="s32"/>'
cXML +=  '    <Cell ss:StyleID="s32"/>'
cXML +=  '    <Cell ss:StyleID="s33"/>'
cXML +=  '    <Cell ss:StyleID="s32"/>'
cXML +=  '    <Cell ss:StyleID="s32"/>'
cXML +=  '   </Row>'

FWrite( hArquivo, cXML , Len(cXML) + 2 )

Return



/*/{Protheus.doc} PrepFim
Finaliza rodape do relatorio

@type       Static Function
@author     Mauro Paladini
/*/

Static Function PrepFim( hArquivo ) 

Local cXML  := ''

cXML +=  '  </Table>'
cXML +=  '  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'
cXML +=  '   <PageSetup>'
cXML +=  '    <Layout x:Orientation="Landscape"/>'
cXML +=  '    <Header x:Margin="0.3"/>'
cXML +=  '    <Footer x:Margin="0.3"/>'
cXML +=  '    <PageMargins x:Bottom="0.75" x:Left="0.25" x:Right="0.25" x:Top="0.75"/>'
cXML +=  '   </PageSetup>'
cXML +=  '   <FitToPage/>'
cXML +=  '   <Print>'
cXML +=  '    <ValidPrinterInfo/>'
cXML +=  '    <PaperSizeIndex>9</PaperSizeIndex>'
cXML +=  '    <Scale>57</Scale>'
cXML +=  '    <HorizontalResolution>600</HorizontalResolution>'
cXML +=  '    <VerticalResolution>600</VerticalResolution>'
cXML +=  '   </Print>'
cXML +=  '   <Zoom>85</Zoom>'
cXML +=  '   <Selected/>'
cXML +=  '   <Panes>'
cXML +=  '    <Pane>'
cXML +=  '     <Number>3</Number>'
cXML +=  '     <ActiveRow>23</ActiveRow>'
cXML +=  '     <ActiveCol>3</ActiveCol>'
cXML +=  '    </Pane>'
cXML +=  '   </Panes>'
cXML +=  '   <ProtectObjects>False</ProtectObjects>'
cXML +=  '   <ProtectScenarios>False</ProtectScenarios>'
cXML +=  '  </WorksheetOptions>'
cXML +=  ' </Worksheet>'
cXML +=  ' <Worksheet ss:Name="Sheet2">'
cXML +=  '  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"'
cXML +=  '   x:FullRows="1" ss:DefaultRowHeight="15">'
cXML +=  '  </Table>'
cXML +=  '  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'
cXML +=  '   <PageSetup>'
cXML +=  '    <Header x:Margin="0.3"/>'
cXML +=  '    <Footer x:Margin="0.3"/>'
cXML +=  '    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>'
cXML +=  '   </PageSetup>'
cXML +=  '   <ProtectObjects>False</ProtectObjects>'
cXML +=  '   <ProtectScenarios>False</ProtectScenarios>'
cXML +=  '  </WorksheetOptions>'
cXML +=  ' </Worksheet>'
cXML +=  ' <Worksheet ss:Name="Sheet3">'
cXML +=  '  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"'
cXML +=  '   x:FullRows="1" ss:DefaultRowHeight="15">'
cXML +=  '  </Table>'
cXML +=  '  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'
cXML +=  '   <PageSetup>'
cXML +=  '    <Header x:Margin="0.3"/>'
cXML +=  '    <Footer x:Margin="0.3"/>'
cXML +=  '    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>'
cXML +=  '   </PageSetup>'
cXML +=  '   <ProtectObjects>False</ProtectObjects>'
cXML +=  '   <ProtectScenarios>False</ProtectScenarios>'
cXML +=  '  </WorksheetOptions>'
cXML +=  ' </Worksheet>'
cXML +=  '</Workbook>'

FWrite( hArquivo, cXML , Len(cXML) + 2 )
FClose( hArquivo )

Return


/*
Log2Array
Abre arquivo de LOG da ExecAuto e retorna em um array
@author Mauro Paladini
@since 10/04/2015
@version 1.0
*/

Static Function Log2Array(cFileLog, lAchaErro )
    
    Local nHdl      := -1
    Local nPos      := 0
    Local nBytes    := 0
    
    Local cLinha    := ""
    Local cChar     := ""

    Local cRet      := ""
    Local aRet      := {}

    Default cFileLog    := ""
    Default lAchaErro   := .T.
    
    IF !Empty(cFileLog)
        nHdl := FOpen(cFileLog)
    Endif

    If !(nHdl == -1)
    
        nBytes := FSeek(nHdl, 0, 2)
        FSeek(nHdl, 0)

        If nBytes > 0

            For nPos := 1 To nBytes
    
                FRead(nHdl, @cChar, 1)
                
                // Quebra de linha (chr(13)+chr(10) = CR+LF)
                If cChar == Chr(10) .Or. nPos == nBytes
                    
                    If lAchaErro .And. "< -- Invalido" $ cLinha
                        cRet    := "Campo -> " + cLinha
                        Exit
                    Endif

                    IF !Empty(cLinha)
                        aAdd(aRet, cLinha)
                    Endif
                
                    cLinha :=  ""
                                         
                ElseIf cChar <> Chr(13)

                    cLinha += cChar

                EndIf
    
            Next nPos

        EndIf
    
        FClose(nHdl)
    
    Endif
        
    IF Empty(cRet) .And.  Len(aRet) >= 2
        cRet := RTrim(aRet[1]) + " => " + RTrim(aRet[Len(aRet)])
    Endif
    
Return IIF( lAchaErro , cRet , aRet )