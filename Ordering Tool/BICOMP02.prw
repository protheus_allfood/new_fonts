#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE "POSCSS.CH"
#INCLUDE "TBICONN.CH"

#DEFINE FW_VIEW_OBJ 3 // Utilizado pelo VIEW

/*CalcbyConsumo
Recalcula os valores com base nos novos consumos informados

@author  Mauro Paladini
@since   20/04/2018
@version 1.0        
*/
User Function CalcbyConsumo()

Local aSaveLines:= FWSaveRows()
Local cIDProc   := P00->P00_ID
Local oView     := FWViewActive()
Local oModel    := FwModelActive()

Local cPeriod   := ''
Local cMesCons  := ''

Local nI        := 0
Local nRet      := 0
Local nNroDias  := 0
Local nQtConDia := 0
Local nQtdCons  := 0
Local nEstAtu   := 0
Local nQtdDif   := 0
Local nEstPrev  := 0

Local aQtdCons
Local nP01Line
Local nP02Line
Local nP04Line
 
Local cProduto
Local dDataSaldo
Local dDTProxEm 

// -- Instancia os modelos 
 
Local oP01DETAIL  := oModel:GetModel("P01DETAIL")
Local oP02DETAIL  := oModel:GetModel("P02DETAIL")
Local oP04DETAIL  := oModel:GetModel("P04DETAIL")

Local nP01Line    := oP01DETAIL:GetLine()
Local nP02Line    := oP02DETAIL:GetLine()
Local nP04Line    := oP04DETAIL:GetLine()

cProduto    := oP01DETAIL:GetValue("P01_COD")
dDTCheg     := oP01DETAIL:GetValue("P01_DTCHEG")
nEstAtu     := oP01DETAIL:GetValue("P01_ESTDIS")

cPeriod     := oP02DETAIL:GetValue("P02_PERIOD")
nRet        := oP02DETAIL:GetValue("P02_QTD")

// -- Consiste se o Periodo est� preenchido

If Empty(AllTrim(cPeriod))
    Return nRet
Endif

// -- Dispara recalculo das proximas entregas 

dDataSaldo  := ddatabase

// -- Busca proximas chegadas

oP04DETAIL:GoLine(1)

IF oP04DETAIL:Length() > 0

    For nI := 1 To oP04DETAIL:Length()
            
        oP04DETAIL:GoLine(nI) 
        
        dDTProxEm   := oP04DETAIL:GetValue('P04_DTPRV')
        cMesCons    := Left( DTOS( dDTProxEm ) , 6 )   // 2018 fev
        nNroDias    := dDTProxEm - dDataSaldo         
        aQtdCons    := StatiCCall( BICOMP01 , GridConsumo , cIDProc , cProduto , dDTProxEm )        
        nQtdCons    := aQtdCons[1]
        nQtConDia   := aQtdCons[2]
        
        IF ( nEstAtu - ( nQtConDia * nNroDias ) ) <= 0        
            nQtdDif     := ( nEstAtu - ( nQtConDia * nNroDias ) ) 
            nEstPrev    := oP04DETAIL:GetValue('P04_QTD')
        Else                
            nQtdDif     := 0
            nEstPrev    := nEstAtu - ( nQtConDia * nNroDias ) + oP04DETAIL:GetValue('P04_QTD')                    
        Endif 
                
        oP04DETAIL:SetValue('P04_DIAS'    , nNroDias    )        
        oP04DETAIL:SetValue('P04_CONSUM'  , ( nQtConDia * nNroDias )    )
        oP04DETAIL:SetValue('P04_QCC'     , ( nQtConDia * 30 )    )
        oP04DETAIL:SetValue('P04_QTDDIF'  , nQtdDif     )
        oP04DETAIL:SetValue('P04_QDISP'   , nEstPrev    ) 
            
        dDataSaldo  := dDTProxEm
        nEstAtu     := nEstPrev

    Next nI
            
Endif

// -- Recalcula cobertura para item posicionafdo 

StatiCCall( BICOMP01 , RetCobert , oModel , .T. )

oP04DETAIL:GoLine(1) 
oView:Refresh()

FWRestRows(aSaveLines)

Return nRet 