#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"


/*/{Protheus.doc} BICOMP01
PE padr�o utilizado pela classe MVC
        
@author         MP
@since          02/10/2018
@version        P12      
/*/ 

User Function BICOMP01()

Local aParam     := PARAMIXB
Local xRet       := .T.
Local oObj       := ''
Local cIdPonto   := ''
Local cIdModel   := ''
Local lIsGrid    := .F.
 
Local nLinha     := 0
Local nQtdLinhas := 0
Local cMsg       := ''
Local bTrue      := {|| .T. }
  
If aParam <> NIL
  
   oObj       := aParam[1]
   cIdPonto   := ALLTRIM( UPPER(aParam[2]) )
   cIdModel   := ALLTRIM( UPPER(aParam[3]) )   
   lIsGrid    := ( Len( aParam ) > 3 )

   // -- Retira valida��o do modelo
           
   If cIdPonto == 'MODELPOS'
   
   Elseif cIDPonto == 'MODELPRE'
        
   ElseIf cIdPonto == 'FORMPOS'
                       
   ElseIf cIdPonto == 'FORMLINEPRE'
        
   ElseIf cIdPonto == 'FORMLINEPOS'
           
   ElseIf cIdPonto == 'MODELCOMMITTTS'
    
   ElseIf cIdPonto == 'MODELCOMMITNTTS'
                   
        IF !AtIsRotina("ALCOMNTTS")
            ALCOMNTTS(oObj,cIdPonto,cIdModel)
        Endif      

   ElseIf cIdPonto == 'FORMCOMMITTTSPOS'
         
   ElseIf cIdPonto == 'MODELCANCEL'
                        
   ElseIf cIdPonto == 'BUTTONBAR'
        
   Elseif "MODELVLD" $ cIdPonto  
                     
   EndIf
 
EndIf
 
Return xRet






/*/{Protheus.doc} ALCOMNTTS
Apos a gravacao total do modelo fora do controle de transa��es
        
@author         Mauro Paladini
@since          02/10/2018
@version        P12      
/*/ 

Static Function ALCOMNTTS(oObj,cIdPonto,cIdModel)

    Local aAreas            := { GetArea() }
    Local nOpcao            := oObj:GetOperation()
    Local oModel            := FwModelActive()
    Local lRetFun           := .T.
    Local cFabRefer         := ''
    Local nI
    
    Local oP05DETAIL  := oModel:GetModel("P05DETAIL")
    Local nP05Line    := oP05DETAIL:GetLine()
            
    IF nOpcao <> MODEL_OPERATION_DELETE

        oP05DETAIL:GoLine(1)
        
        IF oP05DETAIL:Length() > 0
        
            For nI := 1 To oP05DETAIL:Length()
                    
                oP05DETAIL:GoLine(nI)                 
                cFabRefer   += RTRIM(oP05DETAIL:GetValue("P05_REFER")) + ' '                  
        
            Next nI
                    
        Endif

    Endif
   
    RecLock('P00',.F.)
        P00->P00_OBS    :=  cFabRefer
    MsUnLock()


    AEval(aAreas, {|x| RestArea(x)})

Return (lRetFun)

