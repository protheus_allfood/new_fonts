#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE "POSCSS.CH"
#INCLUDE "TBICONN.CH"

#DEFINE FW_VIEW_OBJ 3 // Utilizado pelo VIEW

#DEFINE ANA_BITMAP  1
#DEFINE ANA_ITEM    2
#DEFINE ANA_NIVEL1  3
#DEFINE ANA_NIVEL2  4
#DEFINE ANA_NIVEL3  5
#DEFINE ANA_IDREF   6
#DEFINE ANA_RECNO   7

#DEFINE CLR_NIVEL1     Rgb(255,209,163)
#DEFINE CLR_NIVEL2     Rgb(255,255,204)      
#DEFINE CLR_NIVEL3     Rgb(184,204,224)
#DEFINE CLR_NIVEL4     Rgb(189,255,242)
#DEFINE CLR_NIVEL5     Rgb(205,181,205)

#DEFINE lRecompile      If( File(GetClientDir()+"ALLFOOD.INI") , GetPvProfString("BI-COMPRAS","DropProcedures","00",GetClientDir()+"ALLFOOD.INI") == "01" , .F. )
#DEFINE lVerExcel       If( File(GetClientDir()+"ALLFOOD.INI") , GetPvProfString("General","ExcelCheck","00",GetClientDir()+"ALLFOOD.INI") == "00" , .T. )

// -- IDSX-Defini��es

#DEFINE X1TITULO    1
#DEFINE X1TIPO      2
#DEFINE X1TAM       3
#DEFINE X1DECIM     4
#DEFINE X1SXB       5 

// -- Imagens Defini��es
 
Static cCFGLog      := 'bi-compras_procedures_' + Dtos(Date()) + '.log'
 
/*BICOMP01
Sugest�o de compra

@author  Mauro Paladini
@since   20/04/2018
@version 1.0        
*/

User Function UBICOMP1()

Local oBrowse

Private cBMPMAIS     := "PMSMAIS.PNG"
Private cBMPMENOS    := "PMSMENOS.PNG"
Private cBMPLNO      := 'LBNO'
Private cBMPLOK      := 'LBOK'
Private lProcedures  := .T.
Private aRotina      := MenuDef()

Private oArial_06    := TFont():New('Arial',,-06,,.F.,,,,)
Private oArial_07    := TFont():New('Arial',,-07,,.F.,,,,)
Private oArial_08    := TFont():New('Arial',,-08,,.F.,,,,)
Private oArial_08B   := TFont():New('Arial',,-08,,.T.,,,,)
Private oArial_09    := TFont():New('Arial',,-09,,.F.,,,,)
Private oArial_09B   := TFont():New('Arial',,-09,,.T.,,,,)
Private oArial_10    := TFont():New('Arial',,-10,,.F.,,,,)
Private oArial_10B   := TFont():New('Arial',,-10,,.T.,,,,)
Private oArial_11    := TFont():New('Arial',,-11,,.F.,,,,)
Private oArial_11B   := TFont():New('Arial',,-11,,.T.,,,,)
Private oArial_12    := TFont():New('Arial',,-12,,.F.,,,,)
Private oArial_12B   := TFont():New('Arial',,-12,,.T.,,,,)
Private oArial_14    := TFont():New('Arial',,-14,,.F.,,,,)
Private oArial_14B   := TFont():New('Arial',,-14,,.T.,,,,)

Private cCodRefer    := Space(15)
Private oVarRefer    := P00->P00_OBS   

Private cVarProduto  := Space(100) 
Private oVarProduto

Private cBICSS       := "QHeaderView::section { font-family: Arial, Helvetica, sans-serif; font-size: 09px; color: black ; background-image: url(rpo:fw_brw_hdr.png); border: 1px solid #99CCFF; } "+ ;
                        "QTableView{ rowHeight: 5; font-family: Arial, Helvetica, sans-serif; font-size: 10px; alternate-background-color: LightGray ; color: black }" 
                         



Static aCampos  := {} 

// -- Verifica compilacao das Procedures e Views

FWMsgRun( , { ||  BIConfigure() } ,, "Verificando Procedures ... " )
       
oBrowse := FWMBrowse():New()
oBrowse:SetAlias( 'P00' )
oBrowse:SetDescription( 'Central de Compras' )
oBrowse:DisableDetails()
oBrowse:AddStatusColumns( { || BIStatus() }, { || BILegenda() } )
oBrowse:SetCacheView( .F. )
oBrowse:Activate()
    
Return



/*MenuDef
Menu de op��es do Browse

@author  Mauro Paladini
@since   20/04/2018
@version 1.0        
*/
Static Function MenuDef()

Local aRotina   := {} 

ADD OPTION aRotina TITLE OemToAnsi("Pesquisar")                     ACTION "PesqBrw"            OPERATION 1 ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE OemToAnsi("Visualizar")                    ACTION "VIEWDEF.BICOMP01"   OPERATION 2 ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE OemToAnsi("Incluir")                       ACTION "U_BICOMP02"         OPERATION 3 ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE OemToAnsi("Alterar")                       ACTION "VIEWDEF.BICOMP01"   OPERATION 4 ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE OemToAnsi("Excluir")                       ACTION "VIEWDEF.BICOMP01"   OPERATION 5 ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE OemToAnsi("Exportar")                      ACTION "U_BICOMR01"         OPERATION 8 ACCESS 0 // "Visualizar"

Return aRotina
 
 

/*/{Protheus.doc} ModelDef
Cria o modelo de dados 

@type       Static Function
@author     Mauro Paladini
@since      Jul 17, 2018  8:11:18 PM
@version    1.0
@return     oRet

@see (links_or_references)
/*/

Static Function ModelDef()

Local oModel        := Nil
Local oStruP00      := FwFormStruct(1,'P00',,)
Local oStruP01      := FwFormStruct(1,'P01',,)
Local oStruP05      := FwFormStruct(1,'P05',,)
Local oStruP04      := FwFormStruct(1,'P04',,)
Local oStruP02      := FwFormStruct(1,'P02',,) // -- BIStruMeses(1)

Local bCommit       := Nil   
Local bP01PosLine   := {|oModel| P01LinOk(oModel)}

// -- Triggers

oStruP01:AddTrigger( "P01_DTCHEG"   , "P01_DTCHEG"  , {||.T.} /*bPre*/ , {|oModel| DtCheg()     } )  
oStruP01:AddTrigger( "P01_QTDPED"   , "P01_COBERT"  , {||.T.} /*bPre*/ , {|oModel| RetCobert()  } )   
oStruP02:AddTrigger( "P02_QTD"      , "P02_QTD"     , {||.T.} /*bPre*/ , {|oModel| u_CalcbyConsumo()  } )   


// -- Inicializa Model
oModel  := MpFormModel():New('BICOMP01',, /*{|oModel|PosVldMdl(oModel)}*/ ,  /*bCommit*/  ,/*Cancel*/) 


// -- Fields
oModel:AddFields('P00MASTER',,oStruP00,,,)
oModel:SetDescription( 'Sugest�o de Compras' )


// -- Grid Fabricantes
oModel:AddGrid('P05DETAIL','P00MASTER',oStruP05,,,,, /*{|oModel,lLoad|LoadGrid(oModel,lLoad)}*/ )
oModel:SetRelation('P05DETAIL', { {'P05_FILIAL','xFilial("P05")' },{'P05_IDPROC','P00_ID'}  } , P05->(IndexKey(1) ) ) 
oModel:GetModel('P05DETAIL'):SetNoInsertLine( .T. )
oModel:GetModel('P05DETAIL'):SetNoDeleteLine( .T. )


// -- Grid Sugest�o
oModel:AddGrid('P01DETAIL','P05DETAIL',oStruP01,, bP01PosLine ,,, /*{|oModel,lLoad|LoadGrid(oModel,lLoad)}*/ )
oModel:SetRelation('P01DETAIL', { {'P01_FILIAL','xFilial("P01")' },{'P01_IDPROC','P05_IDPROC'} , {'P01_CODFAB','P05_CODFAB'} } , P01->(IndexKey(1) ) ) 
oModel:GetModel('P01DETAIL'):SetOptional( .T. )
oModel:GetModel('P01DETAIL'):SetNoInsertLine( .T. )


// -- Grid Consumo M�dio
oModel:AddGrid('P02DETAIL','P01DETAIL',oStruP02,,,,,  /*{|oModel,lLoad|LoadGrid(oModel,lLoad)}*/ )
oModel:SetRelation('P02DETAIL', { {'P02_FILIAL','xFilial("P02")' },{'P02_IDPROC','P01_IDPROC'} , {'P02_COD','P01_COD'} } , P02->(IndexKey(1) ) ) 
oModel:GetModel('P02DETAIL'):SetOptional( .T. )


// -- Grid Entregas
oModel:AddGrid('P04DETAIL','P01DETAIL',oStruP04,,,,,  /*{|oModel,lLoad|LoadGrid(oModel,lLoad)}*/ )
oModel:SetRelation('P04DETAIL', { {'P04_FILIAL','xFilial("P04")' },{'P04_IDPROC','P01_IDPROC'} , {'P04_COD','P01_COD'} } , P04->(IndexKey(1) ) ) 
oModel:GetModel('P04DETAIL'):SetOptional( .T. )
oModel:GetModel('P04DETAIL'):SetNoInsertLine( .T. )
oModel:GetModel('P04DETAIL'):SetNoDeleteLine( .T. )

// -- Efetuar tratamento para mudar os registros para uma unica linha


// -- Primary Key
oModel:SetPrimaryKey({'P00_FILIAL', 'P00_ID' })

Return oModel 


/*/{Protheus.doc} P01LinOk
Valida��o da linha do Modelo P01

@type 	User Function
@author 	Mauro Paladini
@since 	Sep 4, 2018  12:51:56 AM
@version 1.0
@return 	${return}, ${return_description}

/*/
Static Function P01LinOk( oModel )

Local oModel        := FwModelActive()
Local oView         := FWViewActive()
Local oP01DETAIL    := oModel:GetModel("P01DETAIL")
Local oP02DETAIL    := oModel:GetModel("P02DETAIL")
Local lRet          := .T.

oP02DETAIL:GoLine(1)
oP02DETAIL:GoLine( oP02DETAIL:Length() )
oView:Refresh() 

Return lRet


/*/{Protheus.doc} ViewDef
Cria a view com todos os objetos da interface

@type       Static Function
@author     Mauro Paladini
@since      Jul 17, 2018  8:11:18 PM
@version    1.0
@return     oRet

@see (links_or_references)
/*/

Static Function ViewDef()

Local oModel        := FwLoadModel('BICOMP01') // -- Carrega model
Local oStruP00      := FwFormStruct(2,'P00',,)

Local oStruP01      := FwFormStruct(2,'P01',,)
Local aCposP01      := oStruP01:GetFields()
Local nST
Local nWidthCpo

Local oStruP05      := FwFormStruct(2,'P05',,)
Local oStruP04      := FwFormStruct(2,'P04',,)
Local oStruP02      := FwFormStruct(2,'P02',,) // --BIStruMeses(2)

Private oPanelGrid  := Nil

oStruP05:RemoveField( "P05_IDPROD" )
oStruP05:RemoveField( "P05_CODFAB" )

// -- Estrutura Consumo Mes a Mes

oStruP02:RemoveField( 'P02_IDPROC' )
oStruP02:RemoveField( 'P02_QTDPES' )


// -- Ajusta tamanho de campos

IF File(GetClientDir()+"ALLFOOD.INI")

    For nST := 1 To Len( aCposP01 )
    
        nWidthDef   := If( File(GetClientDir()+"ALLFOOD.INI") , Val( GetPvProfString( "P01DETAIL" , aCposP01[nST,1] /* cCampo */ , "00" , GetClientDir()+"ALLFOOD.INI") ) , 0 )    
        If nWidthDef <> 0
            nWidthCpo   := nWidthDef    
            oStruP01:SetProperty( aCposP01[nST,1] , MVC_VIEW_WIDTH , nWidthCpo )
        Endif
        
    Next nST
    
Endif


// - Ajusta tamanho do grid fabricantes
oStruP05:SetProperty( 'P05_DESFAB'  , MVC_VIEW_WIDTH , 90 )
oStruP05:SetProperty( 'P05_REFER'   , MVC_VIEW_WIDTH , 90 )


oView     := FWFormView():New()
oView:SetModel( oModel )

// -- Desabilita Barra de bot�es Fechar e Confirmar


oView:AddField( 'VIEW_ABA', oStruP00, 'P00MASTER' )
oView:EnableControlBar(.F.)

oView:AddGrid('PANEL_TREE', oStruP04 , 'P04DETAIL') 
oView:SetViewProperty("PANEL_TREE", "GRIDROWHEIGHT", {20})
oView:EnableTitleView('PANEL_TREE','Importa��es')
oView:AddOtherObject( 'PANEL_EMPTY', {|oPanel| } )                     


// -- Grid dos produtos em sugest�o

oView:AddGrid('PANEL_BROWSE', oStruP01 , 'P01DETAIL') 
oView:SetViewProperty("PANEL_BROWSE", "GRIDROWHEIGHT", {20})
oView:SetViewProperty("PANEL_BROWSE", "CHANGELINE"   , { {|| BIObjects() }} )
oView:EnableTitleView('PANEL_BROWSE','Produtos')

              
//oView:SetViewProperty("PANEL_BROWSE","GRIDSEEK" )   

// -- Grid consumo m�dio dos produtos

oView:AddGrid('PANEL_CONSUMO', oStruP02 , 'P02DETAIL')
oView:SetViewProperty("PANEL_CONSUMO", "GRIDROWHEIGHT", {20})
oView:EnableTitleView('PANEL_CONSUMO','Consumo')

oView:AddGrid('VIEW_FABRIC', oStruP05 , 'P05DETAIL')
oView:SetViewProperty("VIEW_FABRIC", "GRIDROWHEIGHT", {20})
oView:EnableTitleView('VIEW_FABRIC','Fabricantes') 

oView:CreateHorizontalBox( 'INFO_FUNC'      ,00 )
oView:CreateHorizontalBox( 'BAR_BUTTON'     ,07 )   
oView:CreateHorizontalBox( 'EVENTO_FUNC'    ,56 )
oView:CreateHorizontalBox( 'EVENTO_FIM'     ,35 )
oView:CreateHorizontalBox( 'FIM_TELA'       ,00 )   

oView:AddOtherObject("OTHER_PANEL", {|oPanel| BICOMBUT(oPanel)})

oView:CreateVerticalBox( 'INFO_FUNC_ESQ'        , 100   ,'INFO_FUNC' )

oView:CreateVerticalBox( 'EVENTO_FUNC_ESQ'      , 79    ,'EVENTO_FUNC' )
oView:CreateVerticalBox( 'EVENTO_FUNC_CENTER'   , 01    ,'EVENTO_FUNC' )
oView:CreateVerticalBox( 'EVENTO_FUNC_DIR_02'   , 20    ,'EVENTO_FUNC' )
oView:CreateVerticalBox( 'FIM_TELA_EMPTY'       , 100   , 'FIM_TELA' )

oView:CreateHorizontalBox( 'EVENTO_DIR_02_TOPO'     ,55   , 'EVENTO_FUNC_DIR_02' ) 
oView:CreateHorizontalBox( 'EVENTO_DIR_02_MEIO'     ,01    , 'EVENTO_FUNC_DIR_02' ) 
oView:CreateHorizontalBox( 'EVENTO_DIR_02_FIM'      ,44    , 'EVENTO_FUNC_DIR_02' )

oView:SetOwnerView( 'VIEW_ABA'      , 'INFO_FUNC_ESQ' )
oView:SetOwnerView( 'PANEL_BROWSE'  , 'EVENTO_FUNC_ESQ' )

oView:SetOwnerView( 'PANEL_CONSUMO' , 'EVENTO_DIR_02_TOPO' )

oView:SetOwnerView( 'PANEL_TREE'    , 'EVENTO_FIM' )
oView:SetOwnerView( 'VIEW_FABRIC'   , 'EVENTO_DIR_02_FIM' )

oView:SetOwnerView( 'PANEL_EMPTY'   , 'EVENTO_FUNC_CENTER' )
oView:SetOwnerView( 'PANEL_EMPTY'   , 'FIM_TELA_EMPTY' )

oView:SetOwnerView("OTHER_PANEL",'BAR_BUTTON')


// -- Propriedades Visuais
/*
oView:SetViewProperty("PANEL_BROWSE"    , "SETCSS", {cBICSS})   
oView:SetViewProperty("PANEL_CONSUMO"   , "SETCSS", {cBICSS})   
oView:SetViewProperty("PANEL_TREE"      , "SETCSS", {cBICSS})   
oView:SetViewProperty("VIEW_FABRIC"     , "SETCSS", {cBICSS}) 
*/
oView:SetViewProperty('PANEL_BROWSE'    , 'ENABLENEWGRID' )
oView:SetViewProperty('PANEL_CONSUMO'   , 'ENABLENEWGRID' )
oView:SetViewProperty('PANEL_TREE'      , 'ENABLENEWGRID' )
oView:SetViewProperty('VIEW_FABRIC'     , 'ENABLENEWGRID' )

oView:SetViewProperty("PANEL_CONSUMO"   , "GRIDVSCROLL", {.F.})
oView:SetViewProperty("VIEW_FABRIC"     , "GRIDVSCROLL", {.F.})
oView:SetViewProperty("PANEL_TREE"      , "GRIDVSCROLL", {.F.})

oView:SetViewProperty("PANEL_BROWSE"    , "GRIDROWHEIGHT", {5})
oView:SetViewProperty("PANEL_CONSUMO"   , "GRIDROWHEIGHT", {5})   
oView:SetViewProperty("PANEL_TREE"      , "GRIDROWHEIGHT", {5})   
oView:SetViewProperty("VIEW_FABRIC"     , "GRIDROWHEIGHT", {5})     

// -- For�a posi��o do ultimo consumo sempre 

 
Return oView




/*/{Protheus.doc} BIObjects
Executa refresh nos objects

@type       Static Function
@author     Mauro Paladini
@since      Oct 2, 2018  12:43:10 PM
/*/

Static Function BIObjects()

    Local oModel            := FwModelActive()
    Local oP01DETAIL        := oModel:GetModel("P01DETAIL")    

    // -- Alimenta o produto que est� posicionado para auxiliar o usuario na interface
    
    cVarProduto := RTRIM( oP01DETAIL:GetValue('P01_COD') ) + ' ' + RTRIM( oP01DETAIL:GetValue('P01_DESPRO') ) 
    oVarProduto:Refresh()
        
Return�


/*/{Protheus.doc} BICOMBUT
Fun��o para ANCORAR os bot�es no oPanel passado

04/10/18 Mauro      Inicializar a interface ja com o produto posicionado atualizado

@type 	User Function
@author 	Mauro Paladini
@since 	Jul 31, 2018  12:52:48 AM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/
Static Function BICOMBUT( oPanel )

Local nSalto        := 30
Local nDirPos       := 05

Local oModel        := FwModelActive()
Local oP01DETAIL    := oModel:GetModel("P01DETAIL")    


//-- Ancoramos os objetos no oPanel passado
TBtnBmp2():New( 05 , nDirPos , 35, 25,'BUDGET'      ,,,,  {|| MaViewSB2( RetProdGrid() )    },oPanel,"Consulta Estoques" ,,.F.,.F. )
nDirPos += nSalto
 
TBtnBmp2():New( 05 , nDirPos , 35, 25,'S4WB005N'    ,,,,{|| MaComView( RetProdGrid() )      },oPanel,"Consulta Produto" ,,.F.,.F. ) 
nDirPos += nSalto

TBtnBmp2():New( 05 , nDirPos , 35, 25,'PMSEXCEL'    ,,,,{|| BI2Excel( )                     },oPanel,"Enviar p/ Excel" ,,.F.,.F. ) 
nDirPos += ( nSalto * 2 )


// -- Produto  
tSay():New( 006, nDirPos ,{||"Produto:"}, oPanel ,, oArial_12B ,,,,.T.,,,80,20 )
nDirPos += ( nSalto * 1.20 )

oP01DETAIL:GoLine(1)
cVarProduto := RTRIM( oP01DETAIL:GetValue('P01_COD') ) + ' ' + RTRIM( oP01DETAIL:GetValue('P01_DESPRO') ) 

oVarProduto := TGet():New(005, nDirPos, { | u | If( PCount() == 0, cVarProduto , cVarProduto := u ) } , oPanel , 180  , 010, "@!",, 0, 16777215,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"cVarProduto",,,, .T.  )
nDirPos += ( nSalto * 6 )


// -- Referencia 
/*
tSay():New( 006, nDirPos ,{||"Referencia:"}, oPanel ,, oArial_12B ,,,,.T.,,,80,20 )
nDirPos += ( nSalto * 1.5 )

oVarRefer := TGet():New(005, nDirPos, { | u | If( PCount() == 0, cCodRefer , cCodRefer := u ) },oPanel, 070  , 010, "@!",, 0, 16777215,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"cCodRefer",,,, .T.  )
*/
Return NIL




/*/{Protheus.doc} RetProdGrid
Retorna qual o produto posicionado no GRID

@type 	User Function
@author 	Mauro Paladini
@since 	Jul 31, 2018  12:55:04 AM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

Static Function RetProdGrid()

Local cCodProd      := ""
Local oModel        := FwModelActive()
Local oModelGrid    := Nil 

oModelGrid  := oModel:GetModel("P01DETAIL")
cCodProd    := oModelGrid:GetValue("P01_COD")


Return cCodProd




/*/{Protheus.doc} BIStruMeses
Cria estrutura para tabela de consumo m�s a m�s baseada em um SELECT SQL 

@type   User Function
@author     Mauro Paladini
@since  Jul 17, 2018  8:11:18 PM
@version 1.0
@return     oRet

@see (links_or_references)
/*/

Static Function BIStruMeses( nFunOpc )

Local aCampos   := {}
Local nCont     := 0
Local oStru
Local nX        := 0
loCAL aTamQtd   := TamSX3('ZR_QTD')
Local cPict     := PesqPict("SZR",'ZR_QTD')
Local cOrdem    := 'A0'
Local oStruP02  := Nil

Default nFunOpc :=  1

//Quando for chamada para criar Model
If nFunOpc == 1
    
    oStruP02  := FWFormStruct( 1, 'P02', /*bAvalCampo*/,/*lViewUsado*/ )
    
    For nX := 1 To 12

        oStruP02:AddField( ;                      // Ord. Tipo Desc.
        AllTrim( MesExtenso(nX) )        , ;      // [01]  C   Titulo do campo
        AllTrim( MesExtenso(nX) )        , ;      // [02]  C   ToolTip do campo
        'P02_M'+ StrZero(nX,2)            , ;      // [03]  C   Id do Field
        'N'                              , ;      // [04]  C   Tipo do campo
        aTamQtd[1]                       , ;      // [05]  N   Tamanho do campo
        aTamQtd[2]                       , ;      // [06]  N   Decimal do campo
        FwBuildFeature( STRUCT_FEATURE_VALID,"Positivo()"), ;    // [07]  B   Code-block de valida��o do campo
        NIL                              , ;      // [08]  B   Code-block de valida��o When do campo
        {'1=Sim','2=Nao'}                , ;      // [09]  A   Lista de valores permitido do campo
        NIL                              , ;      // [10]  L   Indica se o campo tem preenchimento obrigat�rio
        Nil                              , ;      // [11]  B   Code-block de inicializacao do campo
        NIL                              , ;      // [12]  L   Indica se trata-se de um campo chave
        NIL                              , ;      // [13]  L   Indica se o campo pode receber valor em uma opera��o de update.
        .T.                              )        // [14]  L   Indica se o campo � virtual
    
    Next nX


//Quando for chamada para criar a View
ElseIF nFunOpc == 2

    oStruP02  := FWFormStruct( 2, 'P02', /*bAvalCampo*/,/*lViewUsado*/ )
    
    For nX := 1 To 12
            
        oStruP02:AddField( ; 
        'P02_M' + StrZero(nX,2)       , ;     // [01] C Nome do Campo        
        cOrdem                      , ;     // [02] C Ordem
        AllTrim( MesExtenso(nX) )   , ;     // [03] C Titulo do campo
        AllTrim( MesExtenso(nX) )   , ;     // [04] C Descri��o do campo
        {}                          , ;     // [05] A Array com Help
        'G'                         , ;     // [06] C Tipo do campo
        cPict                       , ;     // [07] C Picture
        Nil                         , ;     // [08] B Bloco de Picture Var
        ''                          , ;     // [09] C Consulta F3
        .T.                         , ;     // [10] L Indica se o campo � evit�vel
        ''                          , ;     // [11] C Pasta do campo
        ''                          , ;     // [12] C Agrupamento do campo
        {}                          , ;     // [13] A Lista de valores permitido do campo
        NIL                         , ;     // [14] N Tamanho Maximo da maior op��o do combo
        NIL                         , ;     // [15] C Inicializador de Browse
        .T.                         , ;     // [16] L Indica se o campo � virtual
        NIL )
        
        cOrdem := Soma1(cOrdem)       
    
    Next NX

EndIF

Return oStruP02




/* BIStatus
Fun��o para buscar status

@author  Mauro Paladini
@since   07/03/2018
@version 1.0
*/
Static Function BIStatus()

Local cStatus   := ''

IF P00->P00_STATUS == '0'
    cStatus := 'BR_BRANCO'

Elseif P00->P00_STATUS == '1'
    cStatus := 'BR_AZUL'

Elseif P00->P00_STATUS == '2'
    cStatus := 'BR_VERDE'

Endif 

Return cStatus



/* BILegenda
Fun��o para exibir legenda

@author  Mauro Paladini
@since   07/03/2018
@version 1.0
*/

Static Function BILegenda()

Local oLegenda  :=  FWLegend():New()

oLegenda:Add( '', 'BR_BRANCO'   , "Planilha calculada" )    
oLegenda:Add( '', 'BR_AZUL'     , "Planilha em an�lise" )   
oLegenda:Add( '', 'BR_VERDE'    , "Planilha finalizada" ) 

oLegenda:Activate()
oLegenda:View()
oLegenda:DeActivate()

Return Nil


 
 
/*/{Protheus.doc} BICOMP02
Processa c�lculo e grava��o dos registros da necessidade de compras

@author	Mauo Paladini 
@since 	14/02/2017
/*/

User Function BICOMP02()

Local aAreas        := { SA1->(GetArea()) , GetArea() , P01->(GetArea()) , P02->(GetArea()) }
Local cPerg         := Padr( Funname() , Len(sx1->x1_GRUPO) )

Local cIDProc       := ""

Private cBI         := 'COM'
Private lProcedures := .T.
Private aProcID     := {}   

Private xFabricDe   := ''
Private xFabricAte  := ''
Private xFornecDe   := ''
Private xFornecAte  := ''
Private xMarcaDe    := ''
Private xMarcaAte   := ''
Private xProdDe     := ''
Private xProdAte    := ''
Private XDtEntDe    := ''
Private XDtEntAte   := ''
Private xArmazen    := GetMV("MV_LOCPAD")
 
// -- Verifica compilacao das Procedures e Views
FWMsgRun( , { ||  BIConfigure() } ,, "Verificando Procedures ... " )

IF !lProcedures
    Return
Endif


// --  Salva variaveis globais (integridade) 
SaveInter()

// -- Cria perguntas no dicionario de dados SX1 
AjustaSX1( cPerg )

// -- Faz chamada inicial de par�metros
IF !Pergunte( cPerg , .T. )
    RestInter()
    Return

Else

    xFabricDe   := MV_PAR01
    xFabricAte  := MV_PAR02
    xFornecDe   := MV_PAR03
    xFornecAte  := MV_PAR04
    xMarcaDe    := MV_PAR05
    xMarcaAte   := MV_PAR06
    xProdDe     := MV_PAR07
    xProdAte    := MV_PAR08
    xDtEntDe    := MV_PAR10 
    XDtEntAte   := MV_PAR11
    xArmazen    := MV_PAR15

Endif

cIDProc := GetNextIDX6()

// -- Exibe interface principal para filtros
nOpcFil := BIFiltro( cIDProc )

IF nOpcFil == 0
    // Help('', 1, 'ALLFOOD',, "Processo cancelado pelo usu�rio" , 1, 0)    
    Return
Endif


// -- Executa Procedures para o calculo das informa��es
 
FWMsgRun( , { ||  BIDataQuery( cIDProc , MV_PAR01 , MV_PAR02 , MV_PAR13, MV_PAR14 , MV_PAR10 , MV_PAR11 , MV_PAR15 ) } ,, "Calculando .. Aguarde  ... " )

// -- Limpa registros temporarios 
BITempClear( cIDProc )

// -- Limpa teclas de atalhos
BIKeysClear()

// --  Salva variaveis globais (integridade) 
RestInter()

// -- Restaura a area
AEval(aAreas, {|x| RestArea(x)})

Return






 
/*/{Protheus.doc} BICOMP03
Edi��o dos registros processados

@author Mauo Paladini 
@since  14/02/2017
/*/

User Function BICOMP03

Local aAreas        := { SA1->(GetArea()) , GetArea() , P01->(GetArea()) , P02->(GetArea()) }
Local cIDProc       := P00->P00_ID
Local cTrb          := GetNextAlias()

Private aFabric     := {}
Private aDialogs    := {}


// -- Salva variaveis globais (integridade) 
SaveInter()


// -- Alimenta array com fabricantes para alimentar as abas do objeto TFOLDER

BEGINSQL ALIAS cTrb

SELECT  P01_CODFAB  AS CODIGO,
        P01_DESFAB  AS NOME
FROM %Table:P01% P01
WHERE P01_FILIAL = %xFilial:P01% AND P01_IDPROC = %Exp:cIDProc% AND P01.D_E_L_E_T_ = '' 
GROUP BY P01_CODFAB, P01_DESFAB

ENDSQL

IF (cTrb)->( !Eof() ) 
    (cTrb)->( DbGoTop() )   
    While (cTrb)->( !Eof() )        
        aAdd( aFabric   , { (cTrb)->CODIGO , RTRIM( (cTrb)->NOME ) } )
        aAdd( aDialogs  , (cTrb)->CODIGO + '-' + RTRIM( (cTrb)->NOME ) )
        (cTrb)->( DbSkip() )
    End
Endif

(cTrb)->( DbCloseArea() )


// -- Abre interface para edi��o
BIDialogShow( cIDProc )

// -- Salva variaveis globais (integridade) 
RestInter()

// -- Restaura a area
AEval(aAreas, {|x| RestArea(x)})

Return




/*/{Protheus.doc} RetCobert()
Trigger Calcula o valor da cobertura

@type 	Static Function
@author 	Mauro Paladini
@since 	Aug 31, 2018  4:04:54 PM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

Static Function RetCobert( oModel , lAuto )

Local nAT       := 0
Local nRet      := 0
Local nDias     := 0
Local nI        := 0
Local nQtdCons  := 0
Local nP01Line  := 0
Local nP04Line  := 0
Local nMeses    := 1
Local nP04Bott  := 0
Local cMesCons  := ''
Local aSaveLines:= FWSaveRows()
Local cIDProc   := P00->P00_ID
Local oView     := FWViewActive()

Local nEstPrev  := 0
Local nQtdCons  := 0
Local nQtConDia := 0
Local aQtdCons
Local nLastEnt
Local dUltEntreg    
Local dPrevChega

Default oModel    := FwModelActive()
Default lAuto     := .F.

// -- Instancia os modelos 
 
oP01DETAIL  := oModel:GetModel("P01DETAIL")
oP02DETAIL  := oModel:GetModel("P02DETAIL")
oP04DETAIL  := oModel:GetModel("P04DETAIL")

nP01Line    := oP01DETAIL:GetLine()
nP04Line    := oP04DETAIL:GetLine()

 
// -- Valida quantidade digitada

IF oP01DETAIL:GetValue("P01_QTDPED") == 0
    Return nRet 
Endif 

// -- Valida se a data prevista de chegada esta preenchida

IF Empty( oP01DETAIL:GetValue("P01_DTCHEG") )
    IF !lAuto
        Help( ,, 'HELP',, 'Data de chegada prevista n�o informada.', 1, 0 )
    Endif
    Return nRet
Endif


// -- Ulitma entrega

IF oP04DETAIL:Length() > 0

    oP04DETAIL:GoLine( oP04DETAIL:Length() )
    dUltEntreg  := oP04DETAIL:GetValue("P04_DTPRV")
    dPrevChega  := oP01DETAIL:GetValue("P01_DTCHEG")
    nEstPrev    := oP04DETAIL:GetValue("P04_QDISP")
    nDias       := ( dPrevChega - dUltEntreg )
    cMesCons  := Str(Year(dPrevChega),4)      + StrZero(Month(dPrevChega),2)

Else

    dUltEntreg  := CTOD("")
    dPrevChega  := oP01DETAIL:GetValue("P01_DTCHEG")
    nDias       := 0
    cMesCons    := Str(Year(dPrevChega),4)      +  StrZero(Month(dPrevChega),2)

Endif

aQtdCons    := GridConsumo( cIDProc , oP01DETAIL:GetValue("P01_COD") , dPrevChega )
nQtdCons    := aQtdCons[1]
nQtConDia   := aQtdCons[2]

// -- Calcular o consumo entre a �ltima entrega prevista e a data da chegada

nConsumo    := nDias * nQtConDia 

// -- Estoque na data de chegada digitada

nEstoque  := ( nEstPrev - nConsumo ) + oP01DETAIL:GetValue("P01_QTDPED") 
nRet      := nEstoque  / nQtConDia 
nRet      := Round( nRet , 2 )

// -- Rec�lculo do Grid do Produto caso a Media seja modificada

If lAuto
    oP01DETAIL:SetValue( "P01_COBERT" , nRet )  
Endif

FWRestRows(aSaveLines)

Return nRet


/*/{Protheus.doc} DtCheg()
Trigger Campo Data de chegada

@type   Static Function
@author Mauro Paladini
@since  Aug 31, 2018  4:04:54 PM
/*/

Static Function DtCheg( oModel )

Local cIDProc     := P00->P00_ID
Local oView       := FWViewActive()
Local oP01DETAIL  := Nil
Local nP01Line    := 0
Local nI
Local dRet        := CTOD("//")
Local dDTCheg     := CTOD("//")

Default oModel    := FwModelActive()

// -- Recupera variaveis 

oP01DETAIL  := oModel:GetModel("P01DETAIL")
nP01Line    := oP01DETAIL:GetLine()
dDTCheg     := oP01DETAIL:GetValue("P01_DTCHEG")
  
// -- Valida se a data prevista de chegada esta preenchida

IF Empty( dDTCheg )
    Help( ,, 'HELP',, 'Data de chegada prevista n�o informada.', 1, 0 )
    Return dRet
Endif

If !Empty( dDTCheg )
    If oP01DETAIL:Length() > 1      
        For nI :=  nP01Line + 1  To oP01DETAIL:Length()        
            oP01DETAIL:GoLine(nI) 
            If !oP01DETAIL:IsDeleted()            
                oP01DETAIL:SetValue( 'P01_DTCHEG' , dDTCheg ) 
            Endif             
        Next nI        
    EndIf
Endif
    
// oView:Refresh()
oP01DETAIL:GoLine(nP01Line)

Return dRet



// -- IDVI-Fun��es de interface Visual --



/*/{Protheus.doc} BIDialogShow
Funcao que exibe a interface principal da ferramenta

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BIDialogShow( cIDProc )

Local oLayer        := Nil
Local aSizeAut      := MsAdvSize()
Local cTitTela      := "Gest�o de Compras"


// -- MSNEWGETDADOS

Local aNoFields     := {}
Local cTrb          := ""
Local cSeek         := ""    
Local cWhile        := ""    
Local cQuery        := "" 
Local nStyleGD      := GD_INSERT+GD_UPDATE+GD_DELETE

Local nX            := 0
Local cButton       := ""
Local cDuploClick   := ""
Local cBackColor    := ""

Local aField        := {}
Local aCposAlter    := {}

Local oOk           := LoadBitmap( GetResources(), "LBOk" ) 
Local oNo           := LoadBitmap( GetResources(), "LBNO" )

// -- Controle

Private oPanFolder  := Nil

Private aObjs       := {}
Private aFolder     := {}
Private aPanel      := {}
Private aPanRef     := {}
Private aGetdados   := {}
Private aRefGetD    := {}
Private aLayer      := {}
Private aPanGrid01  := {}

Private aPosPanTot  := {}
Private aDlgButton  := {}

Private GDaHeader   := {}
Private GDaCols     := {}
Private aYesFields  := {}



// -- Bot�es

Aadd( aDlgButton , { "OK"        , "{|| oDialog:End() }"    , "Sair" } )



// -- MSDIALOG

DEFINE MSDIALOG oDialog FROM 0,0 TO aSizeAut[6],aSizeAut[5] TITLE cTitTela OF oMainWnd PIXEL
    
    oDialog:LCENTERED       := .T.
    oDialog:LMAXIMIZED      := .T.
                                   
    // -- Painel Geral
       
    oLayer := FWLayer():new()
    oLayer:init(oDialog,.F.)
    
    oLayer:addLine( 'LIN_01' , 75 , .T. )
    
    oLayer:addCollumn( 'COL_01' , 80 ,, 'LIN_01' )
    oLayer:addCollumn( 'COL_02' , 20 ,, 'LIN_01' )
    
    oLayer:AddWindow( 'COL_01' , 'L1_WIN01' , "Necessidade de Compras"  , 100, .F., .T., {|| } , "LIN_01" )
    oLayer:AddWindow( 'COL_02' , 'L1_WIN01' , "Previstas"               , 100, .F., .T., {|| } , "LIN_01" )
    
    oLayer:addLine( 'LIN_02' , 25 , .T. )
    oLayer:addCollumn( 'COL_01' , 100 ,, 'LIN_02' )
    oLayer:AddWindow( 'COL_01' , 'L1_WIN01' , "Consumo m�s � m�s" , 100, .F., .T., {|| } , "LIN_02" )
    
    // -- PANEL

    oPanFolder  := oLayer:GetWinPanel('COL_01','L1_WIN01','LIN_01')
    oPanRight   := oLayer:GetWinPanel('COL_02','L1_WIN01','LIN_01')
    oPanBottom  := oLayer:GetWinPanel('COL_01','L1_WIN01','LIN_02')
   
    
    // -- Button bar 
    
    oBtBar := TBar():New( oPanFolder , 20, 30, .T.,,,, .F. )    

    For nX := 1 To Len(aDlgButton)
        cButton := " TBtnBmp2():New( 00, 00, 25, 15, '" + aDlgButton[nX][1] + "' ,,,, " + aDlgButton[nX][2] + ",  oBtBar, '" + aDlgButton[nX][3] + "',, .F., .F. ) "
        &(cButton)
    Next nX
    
    
    // -- TFOLDER
 
    AAdd(aObjs      , 'TFolder():New( 0 , 0 ,aDialogs,,oPanFolder,,,,.T.,, 0,0 )' )
    aAdd(aFolder    , &(aObjs[Len(aObjs)]) )
    
    aFolder[Len(aFolder)]:Align := CONTROL_ALIGN_ALLCLIENT
    
    // -- MSNEWGETDADOS
    
    For nX := 1 To Len(aFabric)
    
        // -- TPANEL para o Grid
        
        AAdd(aObjs      , 'TPanel():New(0,0,"",aFolder[Len(aFolder)]:aDialogs[' + cValToChar(nX) + '],,,,, ' + cValToChar(CLR_WHITE) + ',0,0)' )  
        aAdd(aPanel     , &(aObjs[Len(aObjs)]) ) 
        aAdd(aPanRef    , "GD" + aFabric[nX][1]  )
        
        aPanel[Len(aPanel)]:Align := CONTROL_ALIGN_ALLCLIENT
        
           
        // -- MSNEWGETDADOS

        GDaHeader   := {}
        GDaCols     := {}
        
        aYesFields  := {"P01_COD","P01_ESTDIS","P01_SAIPER","P01_ESTPRE","P01_QTDPED"}
                
        cTrb        := GetNextAlias()   
        cSeek       := xFilial("P01") + cIDProc + aFabric[nX][1]                    
        cWhile      := "P01->P01_FILIAL + P01->P01_IDPROC + P01->P01_CODFAB"            
        cQuery      := "SELECT * FROM " + RETSQLNAME("P01") + " P01 WHERE P01_FILIAL = '" + xFilial("P01") + "' AND P01_IDPROC = '" + cIDProc + "' AND P01_CODFAB = '" + aFabric[nX][1] + "' AND P01.D_E_L_E_T_ = '' " 
        
        FillGetDados(2,"P01",1,cSeek,{|| &cWhile },,aNoFields,/*aYesFields*/,,cQuery,,,@GDaHeader,@GDaCols, /*{|aColsX|  After(aColsX) }*/,, /*{|aHeaderX| AfterH(aHeaderX)} */, cTrb )
        
        AAdd( aObjs     , 'MsNewGetDados():New(0,0,0,0,' + cValToChar(nStyleGD) + ',,,,aYesFields,,9999,,,, aPanel[Len(aPanel)] ,GDaHeader,GDaCols)' )

        aAdd( aGetdados , &(aObjs[Len(aObjs)]) )
        aAdd( aRefGetD  , "GD" + aFabric[nX][1] )
        
        aGetdados[Len(aGetdados)]:SetWtDisable( .T. )   
        aGetdados[Len(aGetdados)]:oBrowse:Align         := CONTROL_ALIGN_ALLCLIENT  
        
                      
        // -- Constru��o TPANEL para Totais
        
        /*
        AAdd(aObjs      , 'TPanel():New(0, 0, "",aFolder[Len(aFolder)]:aDialogs[' + cValToChar(nX) + '],  , .F., .F., Nil , ' + cValToChar(CLR_WHITE) + ' , 0,23, .T., .F. )' ) 
        aAdd(aPanel     , &(aObjs[Len(aObjs)]) ) 
        aAdd(aPanRef    , "TOT" + aFabric[nX,1] )
        
        aPanel[Len(aPanel)]:Align := CONTROL_ALIGN_BOTTOM
        aPanel[Len(aPanel)]:nClrPane := CLR_WHITE
        
        aAdd( aPosPanTot ,  Len(aPanel) )
        */
                                    
    Next nX 
    
    // -- Consumo Mes � Mes

    /*
	oTIBrowser := TIBrowser():New(0,0,100,170, "http://localhost:9093/supply/bootstrapdemo.html", oPanBottom )
    oTIBrowser:Align := CONTROL_ALIGN_ALLCLIENT */

    // -- Entregas Previstas

	cTextSVG	:= MemoRead('entregas.html')
	CONOUT(cTextSVG)
	
	oTIBrowser 	:= TSVG():New(00,00,oPanRight,100,170,cTextSVG)
	oTIBrowser:Align := CONTROL_ALIGN_ALLCLIENT
    
	/*	
    oTIBrowser := TIBrowser():New(0,0,100,170, "http://localhost:9093/supply/entregas.html", oPanRight )
    oTIBrowser:Align := CONTROL_ALIGN_ALLCLIENT   */


    
ACTIVATE MSDIALOG oDialog CENTERED

Return



/*/{Protheus.doc} BIFiltro
Interface com cores para multiplas selecoes de produtos/fabricantes

04/10/18 Mauro  Incluso de pesquisa de produtos

@type 	User Function
@author 	Mauro Paladini
@since 	Jul 31, 2018  12:43:28 AM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

Static Function BIFiltro( cIDProc )

Local oDlg
Local cBitmap       := ''
Local aDlgButton    := {}
Local lMarkAll      := .F.
Local bColor        := Nil
Local nX            := 0
Local nGetOpc       := 0
Local aCpos         := { "P03_MARK" , "P03_BMP" , "P03_ITEM" }

Local aSizeAut      := MsAdvSize( .F. , SetMDIChild() )
Local cTitTela      := "Filtro por sele��o" 
Local cTabFiltro    := 'P03' 
Local cProcName     := ''
Local cVarSeek      := Space(150)
Local bRefresh      := { || MaExecPesq( cVarSeek , oGetFiltro ) }


Private oPanel      := Nil
Private oGetFiltro  := Nil
Private nOpcFil     := 0
Private aArrayAna   := {}
Private aColsRef    := {}
Private aCols       := {}
Private aHeader     := {}

// -- Prepara aHeader

Aadd( aHeader , { ""        ,"P03_MARK"     , "@BMP"    , 15, 0, "", "", "C", "", ""})
Aadd( aHeader , { ""        ,"P03_BMP"      , "@BMP"    , 15, 0, "", "", "C", "", ""})
Aadd( aHeader , { "Item"    ,"P03_ITEM"     , "@!"      , 15, 0, "", "", "C", "", ""})

u_WriteLog( cCFGLog  , '[BIFiltro] cIdProc := ' + cIDProc      ,.T.,.T. )

// -- Cria tabela para uso do filtro 

cProcName  := "SP_BICOM_POPULADW_" + cEmpAnt
IF u_SysObject( cProcName , 'P' )
 
    aResult := TCSPEXEC( cProcName ,;
                         cIDProc ,;
                         xFabricDe ,;
                         xFabricAte ,;
                         XMarcaDe ,;
                         xMarcaAte ,;
                         xProdDe ,;
                         xProdAte )
                            
    TcRefresh( RETSQLNAME("P03") )
    
Else
    Final( "Procedure " + cProcName +  " n�o encontrada" )
Endif



// -- Carrega objeto para filtro na primeira tela

P03->( DbSetOrder(1) )

IF P03->( DbSeek( xFilial("P03") + cIDProc ) )

    While P03->( P03_FILIAL+P03_IDPROC == xFilial("P03") + cIDProc )
            
        // -- Regra pra definir se Bitmap de expandir ou colapsar
        IF Empty(P03->P03_NIVEL3)               
            cBitmap := cBMPMENOS        
        Else   
            cBitmap := ""
        Endif
        
        aAdd( aArrayAna, {  cBitmap ,;
                               RTrim(P03->P03_ITEM) ,;
                               P03->P03_NIVEL1 ,;
                               P03->P03_NIVEL2 ,;
                               P03->P03_NIVEL3 ,;
                               P03->P03_IDREF ,;
                               P03->( Recno() ) } )
        
        aAdd( aCols, {  cBMPLNO ,;
                        cBitmap ,;
                        RTRIM(P03->P03_ITEM) ,;
                        .F. })

        aAdd( aColsRef, P03->P03_IDREF )
                                                
        P03->( DbSkip() )
        
    End  
      
Endif

IF Len(aCols) == 0
    Help('', 1, 'BICOMP01',, "N�o h� dados para exibi��o do grid de sele��o de filtro" +  CRLF , 1, 0)
Endif


// -- Interface para selecao

DEFINE MSDIALOG oDialog FROM 0,0 TO aSizeAut[6],aSizeAut[5] TITLE cTitTela OF oMainWnd PIXEL
    
    // -- PAnel 
    
    oPanel      := TPanel():New(0,0,"",oDlg,,,,,/*CLR_WHITE*/,0,0)
    oPanel:Align := CONTROL_ALIGN_ALLCLIENT
        
    // -- Barra de bot�es

    Aadd( aDlgButton , { "OK"        , "{|| nOpcFil := 1 , oDialog:End() }"    , "Confirmar" } )
    Aadd( aDlgButton , { "CANCEL"    , "{|| nOpcFil := 0 , oDialog:End() }"    , "Cancelar" } )    

    oBtBar := TBar():New( oPanel , 20, 30, .T.,,,, .F. )    

    For nX := 1 To Len(aDlgButton)
        cButton := " TBtnBmp2():New( 00, 00, 25, 15, '" + aDlgButton[nX][1] + "' ,,,, " + aDlgButton[nX][2] + ",  oBtBar, '" + aDlgButton[nX][3] + "',, .F., .F. ) "
        &(cButton)
    Next nX
    

    // -- Pesquisa de Produos
    
    tSay():New(021, 110 ,{||"Pesquisar:" },oPanel,,oArial_12B,,,,.T.,,,80,12 )
    oGetPesquisa := TGet():New(018, 150, { | u | If( PCount() == 0, cVarSeek , cVarSeek := u ) },oPanel, 100,  010, "@!",, 0, 16777215,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,'' /*F3*/,"cVarSeek",,,, .T.  )

    oBtnPesquisar := TButton():New(018, 255,"Pesquisar"      ,oPanel, bRefresh    , 048,12,,,,.T.,,,,,,)
    oBtnPesquisa:cToolTip    :=  'Realize uma pesquisa'
    

    // -- MsNewGetdados
    
    oGetFiltro  := MsNewGetDados():New(033,006,(oPanel:nClientHeight/2)-30,(oPanel:nClientWidth/2)-6,nGetOpc,,,,/*aYesFields*/,,9999,,,,oPanel,aHeader,aCols)
    oGetFiltro:oBrowse:bLDblClick   :=  {|| CursorWait() , DuploClick( @oGetFiltro , cIDProc , 1 /*nPosMark*/ , oGetFiltro:oBrowse:nAt /*nLin*/ ) , CursorArrow() , oGetFiltro:oBrowse:Refresh() }
            
    bColor := {||   IIF( Left( oGetFiltro:aCols[oGetFiltro:oBrowse:nAt,3] ,1) <> Space(1) .And. !Empty(oGetFiltro:aCols[oGetFiltro:oBrowse:nAt,2]) , CLR_NIVEL1 ,;
                    IIF( Left( oGetFiltro:aCols[oGetFiltro:oBrowse:nAt,3] ,1) == Space(1) .And. !Empty(oGetFiltro:aCols[oGetFiltro:oBrowse:nAt,2]) , CLR_NIVEL2 ,  CLR_NIVEL3 ) ) }
    
    oGetFiltro:oBrowse:SetBlkBackColor(bColor)   
    oGetFiltro:oBrowse:aColSizes := { 5,5,100 }   

    @ (oPanel:nClientHeight/2)-27,005 CHECKBOX oChk VAR lMarkAll PROMPT "Marca/Desmarca Todos" SIZE 100,008 PIXEL OF oPanel ON CLICK( CursorWait() , DuploClick( @oGetFiltro , cIDProc , 1 /*nPosMark*/ , oGetFiltro:oBrowse:nAt /*nLin*/ , lMarkAll ) , CursorArrow() , oGetFiltro:oBrowse:Refresh() )
       
    // -- oGetFiltro:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
   
ACTIVATE MSDIALOG oDialog CENTERED



// -- Elimina os registros que n�o foram selecionados 
// -- Elimina itens n�o selecionados com X 

cProcName := 'SP_BICOM_LIMPADW_' + cEmpAnt

IF u_SysObject( cProcName , 'P' ) 
    aResult := TCSPEXEC(    cProcName ,; 
                            xFilial('P03') ,;
                            cIDProc ,;
                            Space(1)  )
                               
    TcRefresh( RETSQLNAME("P03") )
Else
    Final( "Procedure " + cProcName +  " n�o encontrada" )
Endif

// -- Caso o usu�rio cancele a sele��o devemos eliminar inclusive os 
// -- registros selecionados

If nOpcFil == 0
    IF u_SysObject( cProcName , 'P' ) 
        aResult := TCSPEXEC(    cProcName ,; 
                                xFilial('P03') ,;
                                cIDProc ,;
                                'X'  )                               
        TcRefresh( RETSQLNAME("P03") )
    Else
        Final( "Procedure " + cProcName +  " n�o encontrada" )
    Endif
Endif

Return( nOpcFil )




/*/{Protheus.doc} MaExecPesq
Funcao auxiliar para pesquisa de Produtos

@author Mauo Paladini 
@since  14/02/2017
/*/


Static Function MaExecPesq(cPesquisa,oProd)

Local nPos          := 0
LOcal nColPos       := 3
Local nBusca        := Iif(oProd:nAt == Len(oProd:aCols) .Or. oProd:nAt == 1, oProd:nAt, oProd:nAt + 1 )

nPos := Ascan(oProd:aCols,{|x| IIF(At(Alltrim(cPesquisa),x[nColPos])>0,.T.,.F.) } , nBusca )
IF nPos == 0
    nPos := Ascan(oProd:aCols,{|x| IIF(At(Alltrim(cPesquisa),x[nColPos])>0,.T.,.F.) } )
EndIF
If nPos > 0
    oProd:GoTo(nPos)
    oProd:oBrowse:aColSizes := { 5,5,100 }
Else
    Help(" ",1,"REGNOIS")
Endif

Return(.T.)



/*/{Protheus.doc} DuploClick
Vincula fun��es das teclas de atalho utilizadas pelos �cones

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function DuploClick( oObj , cIDProc , nPosMark , nLin , lTodos )

Local a         := 0
Local nX        := 0
Local nY        := 0
Local nL        := 0
Local nLinAna   := 0
Local nPosBmp   := 2

Local nColPos   := oObj:oBrowse:nColPos
Local nPosAtu   := oObj:nAT
Local nPosAna   := 0
Local nPosITAna := 0

Local cNivel1   := ''
Local cNivel2   := ''
Local cNivel3   := ''

Local aDel      := {}
Local cTipoReg  := IIF( nColPos == 1 , "M" , IIF( nColPos == 2 , "X" , "" ) )
Local cTipoOper := ''
Local cRefID    := ''
Local cGrupoID  := ''
Local lExpande  := .F.
Local lColapsa  := .F.
Local nLinOri   := nLin
Local lAdded    := .F.

Private aItens    := oGetFiltro:aCols

If lTodos
    cTipoReg    := 'M'
Endif

If Empty(cTipoReg)
    Return
Endif

If nColPos == 2 .And. Empty(AllTrim(oGetFiltro:aCols[nLin][2]))
    Return
Endif 


// -- Identifica o nivel do registro clicado

IF cTipoReg == "X"
    
    // -- EXPANDE ou COLAPSA o n�vrel
     
    lExpande    := ( oGetFiltro:ACOLS[nPosAtu,nPosBmp] == cBMPMAIS )
    lColapsa    := ( oGetFiltro:ACOLS[nPosAtu,nPosBmp] == cBMPMENOS )
    cIDRef      := ''

    P03->( DbSetOrder(1) )
    IF P03->( DbSeek( xFilial("P03") + cIDProc + aColsRef[nPosAtu] ) )        
        cIDRef  := RTrim(P03->P03_NIVEL1) + RTRim(P03->P03_NIVEL2)  
    Endif
    
    IF lColapsa 
    
        oGetFiltro:ACOLS[nPosAtu,nPosBmp] := cBMPMAIS
               
        For nX := nPosAtu+1 To Len(oGetFiltro:ACOLS)
        
            IF cIDRef $ aColsRef[nX] 
                
                TCMarkUsed( cIDProc , cIDRef , .F. /*lMark*/ , .T. /*lDelete*/, .F./*lRestore*/ )
                
                CONOUT("- Deletando -> " + aColsRef[nX] )
                
                aAdd( aDel , nX )
                
            Else
                Exit
            Endif
            
        Next nX
        
        For nY := Len(aDel) To 1 STEP -1
          
            aDel(oGetFiltro:ACOLS       , aDel[nY])
            aDel(aColsRef               , aDel[nY])
            
        Next nY
        
        aSize(oGetFiltro:ACOLS,     Len(oGetFiltro:ACOLS)-Len(aDel))
        aSize(aColsRef,             Len(aColsRef)-Len(aDel))        
                
    Else
    
        oGetFiltro:ACOLS[nLin,nPosBmp] := cBMPMENOS
                
        // -- Identifica n� atual no array anal�tico completo
        nPosAna := Ascan( aArrayAna , { |x| x[6] == aColsRef[nPosAtu] })
        
        For nX := nPosAna + 1  To Len(aArrayAna)
        
            IF RTrim( aColsRef[nPosAtu] ) $ aArrayAna[nX][6] 
            
                // -- Redimensiona arrays
                aSize(oGetFiltro:ACOLS,     Len(oGetFiltro:ACOLS)+1)
                aSize(aColsRef,             Len(aColsRef)+1)   
                
                
                // -- Insere elemento na mesma posi��o
                aIns(oGetFiltro:ACOLS,    nX)
                aIns(aColsRef,            nX)
               
                IF Left(aArrayAna[nX][2],1) $ '01234567890' .Or. Substr(aArrayAna[nX][2],4,1) $ '0123456789'                
                    cBitmap := cBMPMENOS        
                Else   
                    cBitmap := ""
                Endif

                // -- Atualiza dados do array com os dados do array analitico
                oGetFiltro:ACOLS[nX]    :=  {   cBMPLNO ,;
                                                cBitmap ,;
                                                aArrayAna[nX][2] ,;
                                                .F. }
                                               
                aColsRef[nX] := aArrayAna[nX][6]
                
                // -- Restaura registro que foi marcado como deletado da tabela
                
                TCMarkUsed( cIDProc , cIDRef , .F. /*lMark*/ , .F. /*lDelete*/, .T. /*lRestore*/ )
                                
                CONOUT("- Inserindo -> " + aColsRef[nX] )                
                                               
            Else
                Exit
            Endif
                  
        Next nX                        
            
    Endif
    
    oGetFiltro:oBrowse:Refresh()
    oGetFiltro:Refresh()
    
Elseif cTipoReg == "M"

    // -- Marca ou desmarca
    
    If !lTodos
    
        oGetFiltro:aCols[nLin,nPosMark]  := IIF( oGetFiltro:aCols[nLin,nPosMark] == 'LBOK' , 'LBNO' , 'LBOK' )
        
        cRefID      := aColsRef[nLin] 
        lMark       := IIF( oGetFiltro:ACOLS[nLin,nPosMark] == 'LBOK' , .T. , .F. )
        nL          := Ascan( aArrayAna, { |x| RTrim(x[6]) == RTrim(aColsRef[nLin])  })        

        TCMarkUsed( cIDProc , cRefID , lMark , .F. /*lDelete*/, .F. /*lRestore*/ )  
        
        // -- Verifica se o grupo marcado est� COLAPSADO e ent�o EXPANDE
                
        IF ( ! Empty(aArrayAna[nL,3]) .Or. ! Empty(aArrayAna[nL,4]) ) .And. Empty ( RTrim(aArrayAna[nL,5]) )
            
            nLin += 1
            
            cNivel1 := RTrim(aArrayAna[nL,3])
            cNivel2 := RTrim(aArrayAna[nL,4])
            cNivel3 := RTrim(aArrayAna[nL,5])
                                           
            For nLinAna := nL To Len(aArrayAna)
            
                IF ( cNivel1+cNivel2 ) $ RTrim( aArrayAna[nLinAna,6] )
                                                         
                    nPosITAna   := Ascan( oGetFiltro:aCols , { |x| x[3] == aArrayAna[nLinAna,2]  })
                    cRefID      := aArrayAna[nLinAna,6]
                    
                    If nPosITAna <> 0
                    
                        // -- Item ja esta no GRID atual ent�o somente faz a marca do X 
                        
                        oGetFiltro:aCols[nPosITAna,nPosMark]  := IIF( oGetFiltro:aCols[nPosITAna,nPosMark] == 'LBOK' , 'LBNO' , 'LBOK' )                    
                        TCMarkUsed( cIDProc , cRefID , .T. /*lMark*/ , .F. /*lDelete*/, .F. /*lRestore*/ )
                        
                    Else
                    
                    
                        // -- Redimensiona arrays
                        aSize(oGetFiltro:aCols,     Len(oGetFiltro:ACOLS)+1)
                        aSize(aColsRef,             Len(aColsRef)+1)   
                                                
                        // -- Insere elemento na mesma posi��o
                        aIns(oGetFiltro:aCols,    nLin)
                        aIns(aColsRef,            nLin)
                       
                        IF !Empty(aArrayAna[nLinAna][5])               
                            cBitmap := cBMPMENOS        
                        Else   
                            cBitmap := ""
                        Endif
        
                        // -- Atualiza dados do array com os dados do array analitico                        
                        oGetFiltro:aCols[nLin]    :=  {     cBMPLOK ,;
                                                            ''      ,;
                                                            aArrayAna[nLinAna][2] ,;
                                                            .F. }
                                                       
                        aColsRef[nLin]  := aArrayAna[nLin][6]
                        lAdded          := .T.
    
                        // -- Item esta COLAPSADO ent�o faz inclus�o ( EXPANDE primeiro ) para depois marcar o X                                    
                        TCMarkUsed( cIDProc , cRefID , .T. /*lMark*/ , .F. /*lDelete*/, .T. /*lRestore*/ )                
                    
                    
                    Endif
                
                Endif
                
            Next nLinAna
                        
        Endif
        
        If lAdded
            oGetFiltro:aCols[nLinOri,nPosBmp] := cBMPMENOS        
        Endif

        oGetFiltro:oBrowse:Refresh()
        oGetFiltro:Refresh()        
        
    Else
    
        For a := 1 To Len(oGetFiltro:ACOLS)
            
            oGetFiltro:ACOLS[a,nPosMark]  := IIF( oGetFiltro:ACOLS[a,nPosMark] == 'LBOK' , 'LBNO' , 'LBOK' )
                    
            P03->( DbsetOrder(1) )
            IF P03->( DbSeek( xFilial("P03") + cIDProc + aArrayAna[a,ANA_IDREF] ) )
                RecLock('P03',.F.)
                    P03->P03_MARK   := IIF( oGetFiltro:ACOLS[a,nPosMark] == 'LBOK' , 'X' , '' )
                MsUnLock()     
            Endif
                     
        Next a  
    
    Endif

Endif

Return



/*/{Protheus.doc} BIKeysDef
Vincula fun��es das teclas de atalho utilizadas pelos �cones

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BIKeysDef()

Return


/*/{Protheus.doc} BIKeysClear
Desvincula fun��es das teclas de atalho utilizadas pelos �cones

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BIKeysClear()

Return






// -- IDDB-Fun��es OLAP --


/*/{Protheus.doc} BIDataQuery
Fun��o que realiza o envio de instru��es ao banco de dados
As chamadas devem ser encapsuladas nesta fun��o

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BIDataQuery( cIDProc , xFabricDe , xFabricAte , xDataDe  , xDataAte , xDtEntDe  , xDtEntAte , xArmazen )

Local cProcName     := ''
Local cTrb          := GetNextAlias()
Local aAreas        := 0
Local aLocalIn      := Separa( xArmazen , '-' ) 
Local nX            := 0
Local nDecDef       := TamSX3('P04_QCC')[2]

Begin Transaction

// -- C�lculo do CONSUMO

cProcName   := "SP_BICOM_CONSUMO_" + cEmpAnt
aAdd( aProcID , cIDProc )

IF u_SysObject( cProcName , 'P' ) 

    TcCommit( 5, .T. )   
    aResult := TCSPEXEC( cProcName , cIDProc , xDataDe  , xDataAte , xFabricDe , xFabricAte )   
    TcCommit( 5, .F. )          
    TcRefresh( RETSQLNAME("P02") )

Else

    Final( "Procedure " + cProcName +  " n�o encontrada" )
    
Endif


// -- Incrementa armazens na P06

For nX := 1 To Len(aLocalIn)
    RecLock('P06',.T.)
        P06->P06_FILIAL := xFilial("P06")
        P06->P06_IDPROC := cIDProc
        P06->P06_LOCAL  := aLocalIn[nX]    
    MsUnLock()
Next nX
 

// -- C�lculo da NECESSIDADE

cProcName   := "SP_BICOM_CALC_" + cEmpAnt

aAdd( aProcID , cIDProc )

IF u_SysObject( cProcName , 'P' ) 

    TcCommit( 5, .T. )   
    aResult := TCSPEXEC( cProcName , cIDProc , xDataDe  , xDataAte , xFabricDe , xFabricAte )   
    TcCommit( 5, .F. )          
    TcRefresh( RETSQLNAME("P02") )

Else
    Final( "Procedure " + cProcName +  " n�o encontrada" )
Endif


// -- Busca entregas de importa��o do SIGAEIC

cProcName   := "SP_BICOM_ENTREGAS_" + cEmpAnt

aAdd( aProcID , cIDProc )

IF u_SysObject( cProcName , 'P' ) 

    TcCommit( 5, .T. )   
    aResult := TCSPEXEC( cProcName , cIDProc , '    ' , 'ZZZZ' , xDtEntDe  , xDtEntAte )   
    TcCommit( 5, .F. )          
    TcRefresh( RETSQLNAME("P04") )

Else
    Final( "Procedure " + cProcName +  " n�o encontrada" )
Endif



// -- P05-Alimenta fabricantes utilizados

BEGINSQL Alias cTrb

    SELECT DISTINCT P01_CODFAB, P01_DESFAB
    FROM %table:P01% P01
    WHERE P01_FILIAL = %xFilial:P01% AND P01_IDPROC = %Exp:cIDProc% 
    ORDER BY P01_DESFAB

ENDSQL

(cTrb)->( DbGoTop() )

While (cTrb)->( !Eof() )
    
    RecLock('P05',.T.)
        P05->P05_FILIAL := xFilial('P05') 
        P05->P05_IDPROC := cIDProc
        P05->P05_CODFAB := (cTrb)->P01_CODFAB
        P05->P05_DESFAB := (cTrb)->P01_DESFAB
    MsUnlock()
    
    (cTrb)->( DbSkip() )

End

(cTrb)->( DbCloseArea() )


// -- P04 Atualiza registros com calculos

aAreas      := { P01->(GetArea()) , GetArea() , P02->(GetArea()) , P04->(GetArea()) }

P01->( DbSetOrder(1) )
IF P01->( DbSeek( xFilial('P01') + cIDProc ) )

    While P01->( !EOF() .And. P01_FILIAL+P01_IDPROC == xFilial('P01') + cIDProc )
            
        dDataSaldo  := ddatabase
        
        // -- Busca proximas chegadas
        
        P04->( DbSetOrder(1) )
        IF P04->( DbSeek( xFilial('P04') + cIDProc + P01->P01_COD ) )
        
            nEstAtu     := P01->P01_ESTDIS
        
            While P04->( !EOF() .And. P04_FILIAL+P04_IDPROC+P04_COD == xFilial('P04') + cIDProc + P01->P01_COD )
                            
                cMesCons    := Left( DTOS(P04->P04_DTPRV) , 6 )    // 2018 fev
                nNroDias    := P04->P04_DTPRV - dDataSaldo 
                
                aQtdCons    := BIConsumo( cIDProc , P01->P01_COD , cMesCons )
                nQtdCons    := aQtdCons[1]
                nQtConDia   := aQtdCons[2]
                
                IF ( nEstAtu - ( nQtConDia * nNroDias ) ) <= 0
                    nQtdDif     := ( nEstAtu - ( nQtConDia * nNroDias ) ) 
                    nEstPrev    := P04->P04_QTD
                Else                
                    nQtdDif     := 0
                    nEstPrev    := nEstAtu - ( nQtConDia * nNroDias ) + P04->P04_QTD                    
                Endif 
                
                RecLock("P04",.F.)
                    P04->P04_DIAS   := nNroDias
                    P04->P04_CONSUM := ( nQtConDia * nNroDias )
                    P04->P04_QCC    := Round( nQtConDia * 30 , nDecDef )
                    P04->P04_QTDDIF := nQtdDif
                    P04->P04_QDISP  := nEstPrev  
                MsUnlock() 
            
                dDataSaldo  := P04->P04_DTPRV
                nEstAtu     := nEstPrev
                
                P04->( DbSkip() )
                    
            End

        Endif

        P01->( DbSkip() )
        
    End

Endif
 
AEval(aAreas, {|x| RestArea(x)})


// -- Cabe�alho do documento gerado

RecLock("P00",.T.)

P00->P00_FILIAL := xFilial("P00")
P00->P00_ID     := cIDProc
P00->P00_DTCALC := Date()
P00->P00_HRINC  := Left(Time(),5)
P00->P00_IDUSER := __cUserID
P00->P00_NOMEUS := UsrFullName(__cUserID)
P00->P00_STATUS := '0'

MsUnLock()

End Transaction 

Return





/*/{Protheus.doc} BIConsumo
Retorna consumo por m�s

@type 	User Function
@author 	Mauro Paladini
@since 	Jul 30, 2018  11:15:34 AM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

Static Function BIConsumo( cIDProc, cProduto , cPerEnt )

Local cTrb      := GetNextAlias()
Local aRet      := {}
Local nQTdCons  := 0
Local nMeses    := 0


BEGINSQL Alias cTrb
    SELECT TOP 3 * 
    FROM %table:P02% P02
    WHERE   P02_COD     =   %Exp:cProduto% AND 
            P02_IDPROC  =   %Exp:cIDProc% AND 
            P02_PERIOD  <=  %Exp:cPerEnt%  
    ORDER BY P02_PERIOD DESC
ENDSQL

(cTrb)->( DbGoTop() )

While (cTrb)->( !Eof() )    
    nQtdCons    += (cTrb)->P02_QTD
    nMeses      += 1  
    (cTrb)->( DbSkip() )   
End

(cTrb)->( DbCloseArea())

// -- Alimenta aRet

aAdd( aRet , nQTdCons )
aAdd( aRet , ( nQTdCons / ( 30 * nMeses )  ) )

Return aRet






/*/{Protheus.doc} GridConsumo
Retorna consumo por m�s

@type   User Function
@author Mauro Paladini
@since  Jul 30, 2018  11:15:34 AM

/*/

Static Function GridConsumo( cIDProc, cProduto , dDtCheg  )

Local aSaveLines:= FWSaveRows()
Local oView     := FWViewActive()
Local oModel    := FwModelActive()
Local aRet      := {}
Local dPerIni   := LastDay(dDtCheg) - 90 
Local nI 
Local cConsIni  := Str(Year(dPerIni),4)      + StrZero(Month(dPerIni),2)
Local cConsFim  := Str(Year(dDtCheg),4)      + StrZero(Month(dDtCheg),2)
Local nQtdCons  := 0
Local nMeses    := 0

Default dDtCheg     := CTOD("31/12/49")


// -- Instancia os modelos 
 
oP02DETAIL  := oModel:GetModel("P02DETAIL")

IF oP02DETAIL:Length() > 0
        
    For nI := 1  To oP02DETAIL:Length()
            
        oP02DETAIL:GoLine(nI)
        If !oP02DETAIL:IsDeleted() .And. ( oP02DETAIL:GetValue("P02_PERIOD") >= cConsIni .And. oP02DETAIL:GetValue("P02_PERIOD") <=  cConsFim )            
            nQtdCons    +=  oP02DETAIL:GetValue("P02_QTD")  
            nMeses      += 1                          
        Endif             
    
    Next nI
    
Endif 

aAdd( aRet , nQTdCons )
aAdd( aRet , ( nQTdCons / ( 30 * nMeses )  ) )

Return aRet



/*/{Protheus.doc} BI2Excel
Imprime relat�rio contendo os dados da planilha de compras

@type 	User Function
@author 	Mauro Paladini
@since 	Jul 31, 2018  1:09:06 AM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

Static Function BI2Excel()


Return





/*/{Protheus.doc} BIConfigure()
Fun��o que faz a verifica��o inicial de depend�ncias entre objetos
utilizados dentro do banco de dados.  Procedures, Views.

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BIConfigure()

Local nX        := 0
Local aAllProc  := {}
Local cDirCFG   := '\user_procedures\'
Local cSQLProc  := ''

Local aObjects  := {    { "SP_BICOM_CALC_"      + RTrim(cEmpAnt) , "P" } ,;
                        { "SP_BICOM_CONSUMO_"   + RTrim(cEmpAnt) , "P" } ,;
                        { "SP_BICOM_POPULADW_"  + RTrim(cEmpAnt) , "P" } ,;                                             
                        { "SP_BICOM_LIMPADW_"   + RTrim(cEmpAnt) , "P" } ,;
                        { "SP_BICOM_ENTREGAS_"  + RTrim(cEmpAnt) , "P" } ,;
                        { "VW_CUBOPROD_"        + RTrim(cEmpAnt) , "V" } ,;
                        { 'VW_CUBOVENDAS_'      + RTrim(cEmpAnt) , 'V' } }

// --                         { "SP_BICOM_CHEG_"      + RTrim(cEmpAnt) , "P" } ,;
                        
MontaDir( cDIrCFG )

If File(cCFGLog)
    FErase(cCFGLog)
Endif

// -- Chama componente que verifica e recompila as procedures 

u_WriteLog( cCFGLog  , '[BIConfigure]'      ,.T.,.T. )
u_BIPrepare( aObjects , lRecompile , cCFGLog , cDirCFG )

Return





/*/{Protheus.doc} BITempClear
Atualiza um registro da tabela de sele��o P03 com base no ID do processo e c�digo da refer�ncia

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function TCMarkUsed( cIDProc , cIDRef , lMark , lDelete , lRestore )

Local CSQL  := ''

Default cIDProc     := ''
Default cIDRef      := ''
Default lMark       := .F.
Default lDelete     := .F.
Default lRestore    := .F.

IF !Empty( cIDProc ) .And. !Empty( cIDRef ) 

    IF lMark <> Nil 
        
        cSQL    := "UPDATE " + RETSQLNAME("P03") + " SET "
        cSQL    += "P03_MARK =  '" + IIF( lMark , "X" , "" ) + "' "
        cSQL    += "WHERE P03_FILIAL = '" + xFilial("P03") + "' AND P03_IDPROC = '" + cIDProc + "' AND "
        cSQL    += "P03_IDREF = '" + cIDRef + "' "  
     
    Elseif lDelete <> Nil
    
        cSQL    := "UPDATE " + RETSQLNAME("P03") + " SET "
    
        If lDelete 
            cSQL    += "R_E_C_D_E_L = R_E_C_N_O_ , D_E_L_E_T_ = '*' "
        Else
            cSQL    += "R_E_C_D_E_L = 0  , D_E_L_E_T_ = ' ' "
        Endif
            
        cSQL    += "WHERE P03_FILIAL = '" + xFilial("P03") + "' AND P03_IDPROC = '" + cIDProc + "' AND "
        cSQL    += "P03_IDREF = '" + cIDRef + "' "  
    
    Endif
    
    
    If ( TCSQLExec(cSQL) < 0 )
    
        Aviso( "ERRO [TCMarkUsed P03]" , TCSQLError() , {"Fechar"} , 3 ) 
    
    Endif
        
    TcRefresh( RETSQLNAME("P03") )

Endif

Return Nil



/*/{Protheus.doc} BITempClear
Fun��o que realiza a exclus�o f�sica dos registros tempor�rios utilizados 
durante o processamento.  Este tratamento � necess�rio visando o ganho de performance.

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function BITempClear()

Return






// -- IDSX-Fun��es de dicionario


/*/{Protheus.doc} AjustaSX1
Fun��o que realiza a cria��o das perguntas no dicion�rio SX1

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function AjustaSX1( cPerg )

Local aHelp     := {}
Local aDefPar   := {}


aHelp       := { 'C�digo do fabricante inicial' }  
aDefPar     := { 'Do Fabricante'    , 'C' , TamSX3("Z0_CODIGO")[1] , TamSX3("Z0_CODIGO")[2] , 'SZ0' } 	  
u_XXPutSx1(cPerg,'01', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH1", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR01","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'C�digo do fabricante final' }  
aDefPar     := { 'At� Fabricante'   , 'C' , TamSX3("Z0_CODIGO")[1] , TamSX3("Z0_CODIGO")[2] , 'SZ0' } 
u_XXPutSx1(cPerg,'02', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH2", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR02","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'C�digo do fornecedor final' }
aDefPar     := { 'Do Fornecedor'    , 'C' , TamSX3("A2_COD")[1] , TamSX3("A2_COD")[2] , 'SA2' }
u_XXPutSx1(cPerg,'03', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH3", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR03","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'C�digo do fornecedor final' }
aDefPar     := { 'At� Fornecedor'   , 'C' , TamSX3("A2_COD")[1] , TamSX3("A2_COD")[2] , 'SA2' }
u_XXPutSx1(cPerg,'04', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH4", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR04","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Informe marca inicial (range)' }  
aDefPar     := { 'Da Marca'    , 'C' , TamSX3("P01_CODMAR")[1] , TamSX3("P01_CODMAR")[2] , 'SZ8' }     
u_XXPutSx1(cPerg,'05', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH5", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR05","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Informe marca final (range)' }  
aDefPar     := { 'At� Marca'   , 'C' , TamSX3("P01_CODMAR")[1] , TamSX3("P01_CODMAR")[2] , 'SZ80' } 
u_XXPutSx1(cPerg,'06', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH6", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR06","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Informe produto inicial (range)' }  
aDefPar     := { 'Do Produto'    , 'C' , TamSX3("B1_COD")[1] , TamSX3("B1_COD")[2] , 'SB1' }     
u_XXPutSx1(cPerg,'07', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH7", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR07","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Informe produto final (range)' }  
aDefPar     := { 'At� Produto'   , 'C' , TamSX3("B1_COD")[1] , TamSX3("B1_COD")[2] , 'SB1' } 
u_XXPutSx1(cPerg,'08', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH8", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR08","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

// -- Definir entregas previstas

aHelp       := { 'Informe se deseja visualizar entregas previstas' }  
aDefPar     := { 'Exibir entregas'   , 'N' , 1 , 0 , '' } 
aOpc        := { 'Sim' , 'N�o' , '' }
u_XXPutSx1(cPerg,'09', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CH9", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"C","", aDefPar[X1SXB]  ,"","","MV_PAR09",aOpc[1],aOpc[1],aOpc[1],"",aOpc[2],aOpc[2],aOpc[2],"","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Periodo inicial � considerar nas entregas' }  
aDefPar     := { 'Entregas de'    , 'D' , 8 , 0 , '' }     
u_XXPutSx1(cPerg,'10', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHA", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR10","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Periodo final � considerar nas entregas' }
aDefPar     := { 'Entregas at�'   , 'D' , 8 , 0 , '' } 
u_XXPutSx1(cPerg,'11', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHB", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR11","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

// -- Definr consumos

aHelp       := { 'Informe se deseja visualizar os consumos' }  
aDefPar     := { 'Exibir consumos'   , 'N' , 1 , 0 , '' } 
aOpc        := { 'Sim' , 'N�o' , '' }
u_XXPutSx1(cPerg,'12', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHC", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"C","", aDefPar[X1SXB]  ,"","","MV_PAR12",aOpc[1],aOpc[1],aOpc[1],"",aOpc[2],aOpc[2],aOpc[2],"","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Periodo inicial � considerar p/ os consumos' }  
aDefPar     := { 'Consumo de '    , 'C' , 6 , 0 , '' }     
u_XXPutSx1(cPerg,'13', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHD", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR13","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'Periodo final � considerar p/ os consumos' }
aDefPar     := { 'Consumo at� '   , 'C' , 6 , 0 , '' } 
u_XXPutSx1(cPerg,'14', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHE", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR14","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp       := { 'informe armaz�ns separados por hifem' }  
aDefPar     := { 'Armazen'   , 'C' , 20 , 0 , '' } 
u_XXPutSx1(cPerg,'15', aDefPar[X1TITULO] , ToEng(aDefPar[X1TITULO])  , ToSpa(aDefPar[X1TITULO])  ,"MV_CHF", aDefPar[X1TIPO] , aDefPar[X1TAM] , aDefPar[X1DECIM] ,1,"G","", aDefPar[X1SXB]  ,"","","MV_PAR15","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

Return


/*/{Protheus.doc} ToEng
Fun��o que realiza a tradu��o de uma string para o ingles

@author Mauo Paladini 
@since  10/04/2018
/*/


Static Function ToEng( cPar01 )

Local cRet  := cPar01

Return cRet




/*/{Protheus.doc} ToSpa
Fun��o que realiza a tradu��o de uma string para o Espanhol

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function ToSpa( cPar01 )

Local cRet  := cPar01


Return cRet






